<c:set var="text">
	<fmt:message key="login"/> 
</c:set>
<c:set var="path"><c:url value="/"/></c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	<body>
		<form action="${path}login" method="POST">
		    <fmt:message key="nickName"/>: 
		    <input type="text" name="usuario.nickName" value="${usuario.nickName }" maxlength="10"
		    placeholder="<fmt:message key="nickName"/>" pattern=".{5,10}" title="teste" required />
		    <br />
		    <fmt:message key="senha"/>:  
		    <input type="password" name="usuario.senha" value="${usuario.senha }" maxlength="10"/>
		    <br />
			<button name="_method" value="POST">
				<fmt:message key="ok" />
			</button>
		</form>			
	</body>
</html>	

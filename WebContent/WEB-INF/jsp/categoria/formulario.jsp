<c:set var="text">
	<fmt:message key="salvar"/> 
	<fmt:message key="categoria"/>
</c:set>
<c:set var="rota">
	<c:url value="/categoria"/>
</c:set>
<c:set var="id">
	<c:if test="${categoria != null and categoria.id != null}" >/${categoria.id }</c:if>
</c:set>
	
<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>
		
		<div id="content">
			<h2>${text }</h2>
		<hr />
	
		<fieldset>
			<legend>
				<fmt:message key="info"/> <fmt:message key="categoria"/> 
			</legend>				
				
		<form action="${rota}${id}" method="POST">
				
				<input type="hidden" name="categoria.id" value="${categoria.id }"/>		
				
				<label>
					<fmt:message key="nome"/>:
					<input id="nome" type="text" name="categoria.nome" value="${categoria.nome }"/>	
				</label>
				<br />		
				<label>
					<fmt:message key="descricao"/>:
					<textarea id="descricao" name="categoria.descricao" 
					rows="3" cols="10">${categoria.descricao }</textarea>	
				</label>		
				<br />
			
			<button name="_method" value="${categoria.id == null ? 'POST' : 'PUT'}">
				<fmt:message key="salvar" />
			</button>
		</form>
	</fieldset>		
	</div>
	</body>	
</html>		

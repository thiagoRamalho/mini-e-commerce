<c:choose>
	<c:when test="${not empty categoriaList}">

		<div id="grid">
			<table border="1">
				<tr>
					<th><fmt:message key="check" /></th>
					<!-- <th><fmt:message key="ID" /></th> -->
					<th><fmt:message key="nome" /></th>
					<th><fmt:message key="descricao" /></th>
					<th><fmt:message key="data.cadastro" /></th>
					<th><fmt:message key="data.atualizacao" /></th>										
				</tr>
				<c:forEach items="${categoriaList}" var="categoria" varStatus="s">					
					<tr>
						<td>
							<input type="checkbox" id="checkCategoria" name="listId" value="${categoria.id }" >
							<input type="hidden"  name="categoria.id"  value="${categoria.id }" />							
						</td>						
					
						<td class="nome">${categoria.nome }</td>
						
						<td>${categoria.descricao }</td>
						
						<td class="date">
							<fmt:formatDate type="date" value="${categoria.dataCadastro.getTime()}" />
						</td>
						<td class="date" id="${categoria.id }">
							<fmt:formatDate type="date" value="${categoria.dataAtualizacao.getTime()}" />
						</td>												
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:when>
</c:choose>
<br />

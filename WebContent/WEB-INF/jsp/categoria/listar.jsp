<c:set var="text">
	<fmt:message key="list"/> 
	<fmt:message key="categoria"/>
</c:set>
<c:set var="path"><c:url value="/"/></c:set>
<c:set var="url"><c:url value="categoria/listar/"/></c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>
		<form action="${path }categoria" method="POST">
		
			<jsp:include page="categoryTable.jsp" />
			
			<jsp:include page="../template/crudAction.jsp">
				<jsp:param name="isCriar"  value="true" />	
				<jsp:param name="isEditar" value="${not empty categoriaList }" />	
			</jsp:include> 
		</form>	
				
		<jsp:include page="../template/pagination.jsp" >
			<jsp:param name="url" value="${path}${url}" />
		</jsp:include>
				
	</body>
</html>		

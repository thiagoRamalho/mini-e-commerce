<c:set var="text">
	<fmt:message key="list"/> 
	<fmt:message key="customer"/>
</c:set>
<c:set var="path"><c:url value="/"/></c:set>
<c:set var="url"><c:url value="customer/list/"/></c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>
		<form action="${path }customer" method="POST">
		
			<jsp:include page="customerTable.jsp" />
			
			<jsp:include page="../template/crudAction.jsp">
				<jsp:param name="isShow" value="${not empty customerList }" />
				<jsp:param name="isHiddenDelete" value="true" />	
			</jsp:include>
			
			<jsp:include page="../template/pagination.jsp" >
				<jsp:param name="url" value="${path}${url}" />
		    </jsp:include>								
		</form>	
	</body>
</html>		

<c:choose>
	<c:when test="${not empty customerList}">

		<div id="grid">
			<table border="1">
				<tr>
					<th><fmt:message key="check" /></th>
					<th><fmt:message key="name" /></th>
					<th><fmt:message key="cnpj" /></th>
					<th><fmt:message key="recordDate" /></th>
					<th><fmt:message key="updateDate" /></th>										
				</tr>
				<c:forEach items="${customerList}" var="customer" varStatus="s">					
					<tr>
						<td>
							<input type="checkbox" id="checkCustomer" name="listId" value="${customer.id }" >							
						</td>						
					
						<td class="name">${customer.name }</td>
						
						<td>${customer.cnpj}</td>						
						
						<td class="date">
							<fmt:formatDate type="date" value="${customer.recordDate.getTime()}" />
						</td>
						<td class="date" id="${customer.id }">
							<fmt:formatDate type="date" value="${customer.updateDate.getTime()}" />
						</td>												
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:when>
	<c:otherwise>
		<p><fmt:message key="not.record" /></p>
	</c:otherwise>
</c:choose>
<br />

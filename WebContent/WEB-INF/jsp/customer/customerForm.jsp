<c:set var="text">
	<fmt:message key="save"/> 
	<fmt:message key="customer"/>
</c:set>
<c:set var="path"><c:url value="/"/></c:set>
<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>
		
		<div id="content">
			<h2>${text }</h2>
		<hr />
	
		<fieldset>
			<legend>
				<fmt:message key="info"/> <fmt:message key="tax"/> 
			</legend>				
				
		<form action="${path }customer" method="POST">
			
			<fieldset>
				<legend>
					<fmt:message key="info"/> <fmt:message key="tax"/> 
				</legend>				
				
				<input type="hidden" name="customer.id" value="${customer.id }"/>		
				
				<label>
					<fmt:message key="name"/>:
					<input id="name" type="text" name="customer.name" 
					value="${customer.name }"/>	
				</label>
				<br />		
				<label>
					<fmt:message key="cnpj"/>:
					<input id="cnpj" type="text" name="customer.cnpj" 
					value="${customer.cnpj }" maxlength="14"/>	
				</label>		
				
				<label>
					<fmt:message key="inscEstadual"/>:
					<input id="inscEstadual" type="text" name="customer.inscEstadual" 
					value="${customer.inscEstadual }" maxlength="12"/>	
				</label>
			</fieldset>
			
			<fieldset>
				<legend>
					<fmt:message key="info"/> <fmt:message key="localization"/> 
				</legend>				
			
				<label>
					<fmt:message key="address.street"/>:
					<input id="street" type="text" name="customer.address.street" 
					value="${customer.address.street }"/>	
				</label>		
				<label>
					<fmt:message key="address.number"/>:
					<input id="number" type="text" name="customer.address.number" 
					value="${customer.address.number }"/>	
				</label>
				<br />
				<label>
					<fmt:message key="address.additional"/>:
					<input id="additional" type="text" name="customer.address.additional" 
					value="${customer.address.additional }" maxlength="70"/>	
				</label>		
				<label>
					<fmt:message key="address.zipCode"/>:
					<input id="zipCode" type="text" name="customer.address.zipCode" 
					value="${customer.address.zipCode }" maxlength="8"/>	
				</label>	
				<br />	
				<label>
					<fmt:message key="address.district"/>:
					<input id="district" type="text" name="customer.address.district" 
					value="${customer.address.district }"/>	
				</label>		
				<label>
					<fmt:message key="address.city"/>:
					<input id="city" type="text" name="customer.address.city" 
					value="${customer.address.city }"/>	
				</label>		
			</fieldset>						
			
			<button name="_method" value="POST">
				<fmt:message key="save" />
			</button>
		</form>
	</fieldset>		
	</div>
	</body>	
</html>		

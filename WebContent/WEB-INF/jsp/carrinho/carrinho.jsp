
<html>
	<head>
		<title><fmt:message key="produtos.selecionados"/></title>
	</head>
	
	<body>		
		<div id="content">
			<h2><fmt:message key="produtos.selecionados"/></h2>
			<form action="${path}carrinho" method="POST">
				<fieldset>
					<legend>
						<fmt:message key="info"/> <fmt:message key="produto.plural"/> 
					</legend>	
	
					<jsp:include page="carrinhoTabela.jsp"/>
				</fieldset>
				
				<c:if test="${not empty carrinho.getItens()}">
					<button name="_method" value="DELETE"
						onclick="return confirm('<fmt:message key="confirma.limpar.carrinho"/>')">
						<fmt:message key="limpar" /> <fmt:message key="carrinho" />
					</button>				
					<button name="_method" value="PUT" >
						<fmt:message key="atualizar" /> <fmt:message key="carrinho" />
					</button>
				</c:if>				
			</form>	
		</div>
	</body>	
</html>		

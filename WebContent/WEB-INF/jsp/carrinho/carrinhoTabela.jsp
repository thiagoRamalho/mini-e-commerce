<c:choose>
	<c:when test="${not empty carrinho.getItens()}">
		<div id="grid">
			<table border="1">
				<tr>
					<th><fmt:message key="nome" /></th>
					<th><fmt:message key="quantidade" /></th>
					<th><fmt:message key="preco" /> <fmt:message key="embalagem" /></th>
					<th><fmt:message key="preco" /> <fmt:message key="total" /></th>	
														
				</tr>
				<c:forEach items="${carrinho.getItens()}" var="item" varStatus="s">
				
					<input type="hidden" id="itens" name="itens[${s.index }].produto.id" value="${item.idProduto }" >
					
					<tr>
						<td class="name">
							<a href="${path }produto/${item.idProduto}" >${item.nome }</a>
						</td>
						
						<td>
							<input id="quantidadeSolicitada" type="text" name="itens[${s.index }].quantidadeSolicitada" 
							value="${item.quantidadeSolicitada }" maxlength="3"/>
						</td>
						
						<td>
							<fmt:formatNumber type="currency" value="${item.getPrecoPorEmbalagem()}" maxFractionDigits="2" />
						</td>
							
						<td>
							<fmt:formatNumber type="currency" value="${item.getPrecoTotal()}" maxFractionDigits="2" />  
						</td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="2"></td>
					<td><fmt:message key="total"/></td>
					<td>
						<fmt:formatNumber type="currency" value="${carrinho.getPrecoTotal()}" maxFractionDigits="2" />
					</td>
				</tr>
			</table>
			</div>
		</c:when>
		<c:otherwise>
			<fmt:message key="nao.produto.selecionado"/>
		</c:otherwise>
	</c:choose>	

<c:choose>
	<c:when test="${not empty unidadeList}">

		<div id="grid">
			<table border="1">
				<tr>
					<th><fmt:message key="check" /></th>
					<th><fmt:message key="sigla" /></th>
					<th><fmt:message key="quantidade" /></th>
					<th><fmt:message key="descricao" /></th>
					<th><fmt:message key="data.cadastro" /></th>
					<th><fmt:message key="data.atualizacao" /></th>										
				</tr>
				<c:forEach items="${unidadeList}" var="unidade" varStatus="s">					
					<tr>
						<td>
							<input type="checkbox" id="checkunidade" name="listId" value="${unidade.id }" >							
						</td>						
					
						<td class="name">${unidade.sigla }</td>
						
						<td>${unidade.quantidade}</td>						
						
						<td>${unidade.descricao }</td>
						
						<td class="date">
							<fmt:formatDate type="date" value="${unidade.dataCadastro.getTime()}" />
						</td>
						<td class="date" id="${unidade.id }">
							<fmt:formatDate type="date" value="${unidade.dataAtualizacao.getTime()}" />
						</td>												
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:when>
</c:choose>
<br />

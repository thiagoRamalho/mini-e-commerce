<c:set var="text">
	<fmt:message key="list"/> 
	<fmt:message key="unidade"/>
</c:set>
<c:set var="path"><c:url value="/"/></c:set>
<c:set var="url"><c:url value="unidade/listar/"/></c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>
		<form action="${path }unidade" method="POST">
		
			<jsp:include page="unidadeTable.jsp" />
			
			<jsp:include page="../template/crudAction.jsp">
				<jsp:param name="isCriar"  value="true" />	
				<jsp:param name="isEditar" value="${not empty unidadeList }" />	
			</jsp:include> 
		</form>	
				
		<jsp:include page="../template/pagination.jsp" >
			<jsp:param name="url" value="${path}${url}" />
		</jsp:include>
				
	</body>
</html>		

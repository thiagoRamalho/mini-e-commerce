<c:set var="text">
	<fmt:message key="salvar"/> 
	<fmt:message key="unidade.medida"/>
</c:set>
<c:set var="rota">
	<c:url value="/unidade"/>
</c:set>
<c:set var="id">
	<c:if test="${unidade != null and unidade.id != null}" >/${unidade.id }</c:if>
</c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>		
		<div id="content">
			<h2>${text }</h2>
			<hr />
			
			<fieldset>
				<legend>
					<fmt:message key="info"/> <fmt:message key="unidade.medida"/> 
				</legend>				
			
			<form action="${rota}${id}" method="POST">
							
				<input type="hidden" name="unidade.id" value="${unidade.id }"/>		
				
				<label>	
					<fmt:message key="sigla"/>:
					<input id="sigla" type="text" name="unidade.sigla" value="${unidade.sigla }" maxlength="4"/>	
				</label>
				<br />		
				
				<label>	
					<fmt:message key="quantidade"/>:
					<input id="quantidade" type="text" name="unidade.quantidade" maxlength="7" 
					value="<fmt:formatNumber value='${unidade.quantidade }' maxFractionDigits="2" />"/>  	
				</label>
				<br />		
				
				<label>
					<fmt:message key="descricao"/>:
					<textarea id="descricao" name="unidade.descricao" 
					rows="3" cols="10">${unidade.descricao }</textarea>	
				</label>		
				<br />				
			
			<button name="_method" value="${unidade.id == null ? 'POST' : 'PUT'}">
				<fmt:message key="salvar" />
			</button>
			</form>
		</fieldset>						
		</div>
	</body>	
</html>		

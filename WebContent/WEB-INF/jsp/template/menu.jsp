<c:set var="path"><c:url value="/"/></c:set>

<div id="menu">		
	<ul class="menu">
		<c:forEach items="${session.grupo}" var="grupo">
			<li class="list">
				<a href="#" title="${grupo.description }">${grupo.name }</a>
				<ul class="submenu">
					<c:forEach items="${grupo.links}" var="link">
						<li>
							<a href="<c:url value="/${fn:toLowerCase(grupo.name)}/${fn:toLowerCase(link.name)}"/>" title="${link.description }" >
								${link.name} 
							</a>
						</li>
					</c:forEach>
				</ul>
			</li>
		</c:forEach>
	</ul>
</div>
<br />


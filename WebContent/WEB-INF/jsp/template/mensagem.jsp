<c:if test="${not empty errors}">
	<div id="errorMsg">
		<ul>
			<c:forEach items="${errors}" var="error">	
				<li><fmt:message key="${error.category }"/> - ${error.message }</li>
			</c:forEach>
		</ul>
	</div>
</c:if>
<c:if test="${not empty message}">
	<div id="message">
		<ul>
			<c:forEach items="${message}" var="msg">
				<li><fmt:message key="${msg }"/></li>
			</c:forEach>
		</ul>
	</div>
</c:if>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="path" scope="application"><c:url value="/" /></c:set>
<c:set var="isFornecedor" scope="session">
	${sessaoAplicacao.isLogado() and not sessaoAplicacao.isCliente() }
</c:set>
<html>	
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<title>.:: <fmt:message key="nome.aplicacao"/> - <decorator:title default="<fmt:message key='nome.aplicacao' />" /> ::.</title>		
		<link  href="${path}css/style.css"    rel="stylesheet" type="text/css" media="screen" />
		<script src="${path}javascript/jquery-1.4.2.min.js"   type="text/javascript"></script>			
		<decorator:head/> 	
	</head>
	<body>
		<div id="top">
			<h1><fmt:message key="nome.aplicacao"/></h1>			
		
			<c:if test="${sessaoAplicacao.isLogado()}">			
				<fmt:message key="usuario"/> <fmt:message key="logado"/> : ${sessaoAplicacao.usuario.nickName }
				|
				<a href="<c:url value='/logout'/>">
					<fmt:message key="login.logout" />
				</a>
				<c:if test="${sessaoAplicacao.isCliente()}">			
				|
				<a href="<c:url value='/carrinho'/>"><fmt:message key="carrinho" /></a>
				: ${sessaoAplicacao.getQuantidadeItensCarrinho()} <fmt:message key="produto.plural"/>
				</c:if> 				
			</c:if>		
		</div>

		<hr />

		<div id="content">
			<jsp:include page="mensagem.jsp" />	
			<decorator:body/>
		</div>
		<hr />			
		<div id="footer">
			<jsp:include page="footer.jsp" />
		</div>
	</body>
</html>
<c:if test="${not empty param.url}">
	<div id="pagination">
		
		<c:forEach begin="1" end="${paginador.totalPaginas}" var="page">
			<c:choose>
				<c:when test="${(page eq paginador.paginaAtual)}">
					<small>[${page}]</small>			
				</c:when>
				<c:otherwise>
					<a href="${param.url}${page}" title="${page}">${page}</a>										
				</c:otherwise>				
			</c:choose>										
		</c:forEach>				
	</div>
</c:if>
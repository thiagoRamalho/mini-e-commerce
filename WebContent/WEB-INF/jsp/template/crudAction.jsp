<c:set var="somenteFornecedor" 
value = "${sessaoAplicacao.isLogado() and not sessaoAplicacao.isCliente()}" />

<c:if test="${somenteFornecedor}">
	
	<c:if test="${param.isCriar}">
		<button name="_method" value="GET">
			<fmt:message key="cadastrar" />
		</button>
	</c:if>
	<c:if test="${param.isEditar}">
		<button name="_method" value="PUT">
			<fmt:message key="editar" />
		</button>
	</c:if>
	<c:if test="${param.isRemover}">
		<button name="_method" value="DELETE"
			onclick="return confirm('<fmt:message key="confirm.delete"/>')">
			<fmt:message key="remover" />
		</button>
	</c:if>
</c:if>

<c:set var="text">
	${produto.nome }
</c:set>
<c:set var="rota">
	<c:url value="/carrinho/adicionar/"/>
</c:set>
<c:set var="id">
	<c:if test="${produto != null and produto.id != null}" >${produto.id }</c:if>
</c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>		
		<div id="content">
			<h2>${text }</h2>

			<fieldset>
				<legend>
					<fmt:message key="info"/> <fmt:message key="produto"/> 
				</legend>				
			
			<form action="${rota}${id}" method="POST">				
				<input type="hidden" name="produto.id" value="${produto.id }"/>	
				
				<p>
					<strong><fmt:message key="descricao"/>:</strong>
					${produto.descricao }
				</p>
				<p>
					<strong><fmt:message key="preco"/>:</strong>
					<fmt:formatNumber type="currency" value="${produto.preco}" maxFractionDigits="2" />
				</p>
				<p>
					<strong><fmt:message key="embalagem"/>:</strong>
					${produto.unidade.sigla } - ${produto.unidade.descricao }</p>		
				<p>
					<strong><fmt:message key="preco"/> <fmt:message key="embalagem"/>:</strong>
					<fmt:formatNumber type="currency" value="${produto.preco * produto.unidade.quantidade}" 
					maxFractionDigits="2" />
				</p>
				
				<p>
					<strong><fmt:message key="disponibilidade"/>:</strong>
					<fmt:message key="${produto.disponivel ? 'disponivel' : 'indisponivel' }"/>
				</p>
				
				
				<c:if test="${not isFornecedor and produto.disponivel}">				
					<button name="_method" value="POST">
						<fmt:message key="adicionar.carrinho" />
					</button>
				</c:if>
			</form>	
		</fieldset>						
		</div>
	</body>	
</html>		

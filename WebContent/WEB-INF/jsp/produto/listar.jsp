<c:set var="text">
	<fmt:message key="listar"/> 
	<fmt:message key="produto"/>
</c:set>
<c:set var="path"><c:url value="/"/></c:set>
<c:set var="url"><c:url value="produto/listar/"/></c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>
		<form action="${path }produto" method="POST">
		
			<jsp:include page="produtoTabela.jsp" />
			
			<jsp:include page="../template/crudAction.jsp">
				<jsp:param name="isCriar"    		   value="true" />	
				<jsp:param name="isEditar"   		   value="${not empty produtoList }" />
				<jsp:param name="isRemover"  		   value="${not empty produtoList }" />
				<jsp:param name="isAdicionarCarrinho"  value="${not empty produtoList }" />
			</jsp:include>	
			
			<input type = "hidden" name="pagina" value="${paginador.paginaAtual }" />
										
		</form>
		
		<jsp:include page="../template/pagination.jsp" >
			<jsp:param name="url" value="${path}${url}" />
		</jsp:include>
		
	</body>
</html>		

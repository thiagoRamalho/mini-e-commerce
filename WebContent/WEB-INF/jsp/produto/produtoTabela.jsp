<c:choose>
	<c:when test="${not empty produtoList}">

		<div id="grid">
			<table border="1">
				<tr>
					<c:if test="${isFornecedor}">
						<th><fmt:message key="check" /></th>
					</c:if>
					<th><fmt:message key="nome" /></th>
					<th><fmt:message key="descricao" /></th>
					<th><fmt:message key="categoria" /></th>
					<th><fmt:message key="preco" /></th>
					<th><fmt:message key="embalagem" /></th>
					<th><fmt:message key="preco" /> <fmt:message key="embalagem" /></th>										
					<c:if test="${isFornecedor}">
						<th><fmt:message key="data.cadastro" /></th>
					</c:if>
					<th><fmt:message key="data.atualizacao" /></th>
					<th><fmt:message key="disponibilidade" /></th>										
				</tr>
				<c:forEach items="${produtoList}" var="produto" varStatus="s">
					<input type="hidden"  name="produto.id" value="${produto.id }" />					
					<tr>
						<c:if test="${isFornecedor}">
						<td>
							<input type="checkbox" id="checkproduto" name="listId" value="${produto.id }" >
						</td>						
						</c:if>
												
						<td class="name">
							<a href="${path }produto/${produto.id}" >${produto.nome }</a>
						</td>
						
						<td>${produto.descricao }</td>
						
						<td>${produto.categoria.nome }</td>
						
						<td>
							<fmt:formatNumber type="currency" value="${produto.preco}" maxFractionDigits="2" />
						</td>
						
						<td>${produto.unidade.sigla } - ${produto.unidade.quantidade }</td>
							
						<td>
							<fmt:formatNumber type="currency" value="${produto.preco * produto.unidade.quantidade}" 
							maxFractionDigits="2" />
						</td>
						<c:if test="${isFornecedor}">						
							<td class="date">
								<fmt:formatDate type="date" value="${produto.dataCadastro.getTime()}" />
							</td>
						</c:if>
						<td class="date">
							<fmt:formatDate type="date" value="${produto.dataAtualizacao.getTime()}" />
						</td>
						<c:set var="disponib">${produto.isDisponivel() ? 'disponivel' : 'indisponivel'}</c:set>
						<td><fmt:message key="${disponib}" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:when>
</c:choose>
<br />

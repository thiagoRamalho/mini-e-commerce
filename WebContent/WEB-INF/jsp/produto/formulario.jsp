<c:set var="text">
	<fmt:message key="salvar"/> 
	<fmt:message key="produto"/>
</c:set>
<c:set var="rota">
	<c:url value="/produto"/>
</c:set>
<c:set var="id">
	<c:if test="${produto != null and produto.id != null}" >/${produto.id }</c:if>
</c:set>

<html>
	<head>
		<title>${text}</title>
	</head>
	
	<body>		
		<div id="content">
			<h2>${text }</h2>
			<hr />
			
			<fieldset>
				<legend>
					<fmt:message key="info"/> <fmt:message key="produto"/> 
				</legend>				
			
			<form action="${rota}${id}" method="POST">				
				<input type="hidden" name="produto.id" value="${produto.id }"/>				
				
				<label>	<fmt:message key="nome"/>:
						<input id="nome" type="text" name="produto.nome" value="${produto.nome }"/>	
				</label>
				<br />
						
				<label>
					<fmt:message key="categoria"/>:
					<select name="produto.categoria.id">
	  					<c:forEach items="${categoriaList}" var="categoria">
	      					<option 
	      						<c:if test="${produto.categoria.id == categoria.id}">selected ="true"</c:if> 
	      						value="${categoria.id}">${categoria.nome}</option>
	  					</c:forEach>
					</select>				
					</label>
				<br />
						
				<label>
					<fmt:message key="unidade.medida"/>:
					<select name="produto.unidade.id">
	  					<c:forEach items="${unidadeList}" var="unidade">
	      					<option 
	      						<c:if test="${produto.unidade.id == unidade.id}">selected ="true"</c:if> 
	      						value="${unidade.id}">${unidade.sigla}</option>
	  					</c:forEach>
					</select>						
				</label>
				<br />
						
				<label>
					<fmt:message key="preco"/>:
					<input id="preco" type="text" name="produto.preco" 
					value="${produto.preco }"/>	
				</label>
				<br />
						
				<label>
					<fmt:message key="descricao"/>:
					<textarea id="descricao" name="produto.descricao" 
					rows="3" cols="10">${produto.descricao }</textarea>	
				</label>				
				<br />
				<label>
					<fmt:message key="disponibilidade"/>:
					<input type="radio" name="produto.disponivel" 
					value="true" ${produto.disponivel ? 'checked' : ''} >
					<fmt:message key="disponivel"/>
					
					<input type="radio" name="produto.disponivel" 
					value="false" ${not produto.disponivel ? 'checked' : ''} >
					<fmt:message key="indisponivel"/>
										
				</label>				
				<br />
				
			<button name="_method" value="${produto.id == null ? 'POST' : 'PUT'}">
				<fmt:message key="salvar" />
			</button>
			</form>	
		</fieldset>						
		</div>
	</body>	
</html>		

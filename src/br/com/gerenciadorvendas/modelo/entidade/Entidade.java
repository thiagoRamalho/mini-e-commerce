package br.com.gerenciadorvendas.modelo.entidade;

import java.util.Calendar;

public interface Entidade {

	public Long getId();
	
	public void setDataCadastro(Calendar calendar);
	public void setDataAtualizacao(Calendar calendar);
	
	public Calendar getDataCadastro();
	public Calendar getDataAtualizacao();

}

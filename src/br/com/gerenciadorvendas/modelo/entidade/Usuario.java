package br.com.gerenciadorvendas.modelo.entidade;


import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;

/**ß
 * 
 * @author       $h@rk
 * @Date         31/12/2011
 * @project      GerenciadorVendasCore
 * @Description:
 */
@Entity
public class Usuario implements Cloneable{

	
	@Id @GeneratedValue	
	private Long id;
	
	@NotNull @Size(min=5, max=10)
	private String nickName;
	
	@NotNull @Size(min=5, max=10)
	private String senha;
	
	@Enumerated(EnumType.STRING) 
	private PerfilUsuario perfilUsuario;
	
	@Column(insertable=true, updatable=false)
	private Calendar dataCadastro;
	
	@Column(insertable=true, updatable=true)
	private Calendar dataAlteracao;
	
	public Usuario(){}

	/**
	 * Construtor privado apenas para utilização na clonagem do objeto
	 * @param id
	 * @param nickName
	 * @param perfilUsuario
	 */
	private Usuario(Long id, String nickName , PerfilUsuario perfilUsuario) {
		super();
		this.id = id;
		this.nickName = nickName;
		this.perfilUsuario = perfilUsuario;
	}

	@Override
	public Usuario clone(){
		return new Usuario(id, nickName, perfilUsuario);
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setPerfilUsuario(PerfilUsuario perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public PerfilUsuario getPerfilUsuario() {
		return perfilUsuario;
	}
	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}


}

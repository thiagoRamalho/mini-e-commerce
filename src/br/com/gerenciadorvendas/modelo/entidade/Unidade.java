package br.com.gerenciadorvendas.modelo.entidade;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Classe que modela uma unidade de medida
 * @author Thiago Ramalho
 *
 */
@Entity
public class Unidade implements Entidade{

	
	@Id @GeneratedValue
	private Long id;
	
	@NotNull @Size(min=2, max=4)
	private String sigla;
	
	@NotNull @Min(1)
	private int quantidade;
	
	@NotNull @Size(min=5, max=50)
	private String descricao;
	
	@Column(insertable=true, updatable=false)
	private Calendar dataCadastro;
	
	@Column(insertable=true, updatable=true)
	private Calendar dataAtualizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Calendar dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}
	
}

package br.com.gerenciadorvendas.modelo.entidade;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;


/**
 * Classe que modela um item de pedido
 * @author Thiago Ramalho
 *
 */
//@Entity
public class Item {

	@Id @GeneratedValue
	private Long id;
	
	//produto que foi inserido
	@OneToOne
	private Produto produto;
	
	//quantidade de vezes que o produto esta sendo inserido
	private int quantidadeSolicitada = 1;
	
	//preco de todas as solicitacoes e suas embalagens
	private BigDecimal precoTotal;
	
	public Item(Produto produto) {
		this.produto = produto;
	}

	public int getQuantidadeSolicitada() {
		return quantidadeSolicitada;
	}

	public void setQuantidadeSolicitada(int quantidadeSolicitada) {
		this.quantidadeSolicitada = quantidadeSolicitada;
	}

	public String getNome() {
		return this.produto.getNome();
	}

	public Long getIdProduto() {
		return produto.getId();
	}
	
	
	/**
	 * Retorna o valor total do item levando em conta
	 * o valor por embalagem do produto multiplicado
	 * pelas quantidades solicitadas
	 * 
	 * @return
	 */
	public BigDecimal getPrecoTotal(){
		
		//preco por embalagem
		BigDecimal precoEmbalagem = this.getPrecoPorEmbalagem();
		
		//preco da embalagem pela quantidade solicitada
		this.precoTotal = precoEmbalagem.multiply(new BigDecimal(""+this.quantidadeSolicitada));
		
		return this.precoTotal;
	}
	
	/**
	 * Retorna a quantidade descrita
	 * na sua embalagem
	 * 
	 * @return
	 */
	public int getQuantidadeEmbalagem(){
		return this.produto.getUnidade().getQuantidade();
	}
	
	/**
	 * Retorna o preco do item multiplicado pela
	 * quantidade existente na sua embalagem
	 * 
	 * @return
	 */
	public BigDecimal getPrecoPorEmbalagem(){
		
		BigDecimal quantidadeEmbalagem = new BigDecimal(""+this.getQuantidadeEmbalagem());
		
		return this.produto.getPreco().multiply(quantidadeEmbalagem);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}

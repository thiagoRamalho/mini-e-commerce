package br.com.gerenciadorvendas.modelo.entidade;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe que modela o carrinho de compras
 * 
 * @author Thiago Ramalho
 *
 */
public class Carrinho {
	
	private BigDecimal precoTotal = BigDecimal.ZERO;
	
	private Map<Long, Item> itens = new HashMap<Long, Item>();
	
	public void adicionar(Produto produto){
		
		Item item = new Item(produto);

		//se nao possui o produto na lista...
		if(!this.itens.containsKey(produto.getId())){
			//adiciona ao mapa
			this.itens.put(produto.getId(), item);
		}
		else {
			//se ja possui o item...
			item = this.itens.get(produto.getId());
			
			//eliminamos o valor ja adicionado para esse item para
			//que o calcularPreco nao o readicione
			this.calcularPreco(item.getPrecoTotal().negate());

			int quantidadeAtual = item.getQuantidadeSolicitada();
			//incrementa a quantidade já que se trata do mesmo item
			item.setQuantidadeSolicitada(++quantidadeAtual);
		}
		
		this.calcularPreco(item.getPrecoTotal());
	}

	/**
	 * Atualiza o item no carrinho com base na sua quantidade
	 * caso envie quantidade menor que 1 ira remove-lo
	 * @param item
	 */
	public void atualizar(Item item){

		//guardamos a nova quantidade pois iremos sobreescrever a referencia
		int novaQuantidade = item.getQuantidadeSolicitada();
		
		//recuperamos o item existente no mapa 
		item = this.itens.get(item.getIdProduto());

		//elimina o valor total do item existente do valor total do carrinho
		this.calcularPreco(item.getPrecoTotal().negate());

		//se o item possui alguma quantidade...
		if(novaQuantidade > 0){
			
			//atualizamos a sua quantidade pelo novo valor
			item.setQuantidadeSolicitada(novaQuantidade);
			
			//adicionamos o valor do item ao total do carrinho
			this.calcularPreco(item.getPrecoTotal());
			
		} else {
			//se enviou quantidade zerada elimina do mapa
			this.itens.remove(item.getIdProduto());
		}
	}
	
	private void calcularPreco(BigDecimal precoItem) {
		this.precoTotal = this.precoTotal.add(precoItem);
	}
	
	/**
	 * Limpa o carrinho eliminando itens 
	 * e zerando o preco total
	 */
	public void limpar(){
		this.itens.clear();
		this.precoTotal = BigDecimal.ZERO;
	}
	
	/**
	 * Retorna a quantidade de itens unicos
	 * adicionados
	 * 
	 * @return
	 */
	public int getQuantidadeItens(){
		return this.itens.size();
	}
	
	/**
	 * Retorna colecao de itens adicionados
	 * 
	 * @return
	 */
	public Collection<Item> getItens(){
		return this.itens.values();
	}
	
	/**
	 * Retorna o valor total de itens adicionados
	 * @return
	 */
	public BigDecimal getPrecoTotal(){
		
/*		this.precoTotal = BigDecimal.ZERO;
		
		for (Item item : this.itens.values()) {
			this.precoTotal = this.precoTotal.add(item.getPrecoTotal());
		}
*/		
		return this.precoTotal;
	}
}

package br.com.gerenciadorvendas.modelo.util;

/**
 * @author       $h@rk
 * @Date         31/12/2011
 * @project      GerenciadorVendasCore
 * @Description: Papel dos usu?rios
 */
public enum PerfilUsuario {
	CLIENTE, FORNECEDOR;
}

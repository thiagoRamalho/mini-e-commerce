package br.com.gerenciadorvendas.modelo.util;

import java.util.Arrays;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.gerenciadorvendas.modelo.entidade.Usuario;

/**
 * Cria o banco de dados e já grava os usuários padrão
 * @author Thiago Ramalho
 *
 */
 class CriarBanco {

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");
		EntityManager entityManager = factory.createEntityManager();

		try{

			entityManager.getTransaction().begin();

			String hql = " delete from Usuario u where u.nickName in :lista";

			Query query = entityManager.createQuery(hql);
			
			query.setParameter("lista", Arrays.asList("cliente", "fornecedor"));
			
			query.executeUpdate();
			
			Usuario usuario1 = new Usuario();
			usuario1.setDataAlteracao(Calendar.getInstance());
			usuario1.setDataCadastro(Calendar.getInstance());
			usuario1.setNickName("cliente");
			usuario1.setSenha("cliente");
			usuario1.setPerfilUsuario(PerfilUsuario.CLIENTE);

			Usuario usuario2 = new Usuario();
			usuario2.setDataAlteracao(Calendar.getInstance());
			usuario2.setDataCadastro(Calendar.getInstance());
			usuario2.setNickName("fornecedor");
			usuario2.setSenha("fornecedor");
			usuario2.setPerfilUsuario(PerfilUsuario.FORNECEDOR);
			
			entityManager.persist(usuario1);
			entityManager.persist(usuario2);

			entityManager.getTransaction().commit();

			entityManager.close();

			factory.close();

		}catch(Exception e){
			e.printStackTrace();
			entityManager.getTransaction().rollback();
		}

	}
}



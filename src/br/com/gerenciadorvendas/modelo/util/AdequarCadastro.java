package br.com.gerenciadorvendas.modelo.util;

import java.util.Calendar;

import br.com.caelum.vraptor.ioc.Component;
import br.com.gerenciadorvendas.modelo.entidade.Entidade;

@Component
public class AdequarCadastro {

	public Entidade adicionarData(Entidade entidade){

		if(entidade.getDataCadastro() == null){
			entidade.setDataCadastro(Calendar.getInstance());
		}

		entidade.setDataAtualizacao(Calendar.getInstance());

		return entidade;
	}
}

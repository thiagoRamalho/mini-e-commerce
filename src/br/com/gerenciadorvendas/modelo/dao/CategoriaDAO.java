package br.com.gerenciadorvendas.modelo.dao;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.gerenciadorvendas.modelo.entidade.Categoria;


/**
 * @author       $h@rk
 * @Date         09/11/2011
 * @project      GerenciadorVendasCore
 * @Description: 
 */
@Component
public class CategoriaDAO {

	private final EntityManager em;

	public CategoriaDAO(EntityManager em){
		this.em = em;
	}

	public int buscarTotalRegistros(){		

		String query = "select count(*) from Categoria ";

		TypedQuery<Long>  q = this.em.createQuery(query, Long.class);

		return q.getSingleResult().intValue();	       
	}

	/**
	 * Realiza busca comecando pelo parametro inicio e com o maximo
	 * de elementos enviado no parametro maximo 
	 * @param primeiro
	 * @param maximo
	 * @return
	 */
	public List<Categoria> buscarLimitadoPor(int inicio, int maximo) {

		List<Categoria> list = Collections.emptyList();

		try {

			String hql = " from Categoria c ";

			TypedQuery<Categoria> query = this.em.createQuery(hql, Categoria.class);

			query.setFirstResult(inicio);
			query.setMaxResults(maximo);

			list = query.getResultList();

		} catch (NoResultException e) {
			list = Collections.emptyList();
		}

		return list;
	}


	public void salvar(Categoria categoria){
		this.em.persist(categoria);
	}

	/**
	 * Retorna todas as categorias cadastradas
	 * @return
	 */
	public List<Categoria> buscarTodas(){

		String hql = " from Categoria c ";
		TypedQuery<Categoria> query = this.em.createQuery(hql, Categoria.class);

		return query.getResultList();
	}

	/**
	 * Retorna a categoria compativel com o id enviado
	 * 
	 * @param id
	 * @return
	 */
	public final Categoria buscarPorID(Long id) {
		return this.em.find(Categoria.class, id);
	}

	/**
	 * Buscar por nome
	 * @param nome
	 * @return
	 */
	public List<Categoria> buscarPorNome(String nome) {

		String hql = " from Categoria c where UPPER(TRIM(c.nome)) = :nome";

		TypedQuery<Categoria> query = this.em.createQuery(hql, Categoria.class);

		query.setParameter("nome", nome.toUpperCase().trim());

		return query.getResultList();
	}
}

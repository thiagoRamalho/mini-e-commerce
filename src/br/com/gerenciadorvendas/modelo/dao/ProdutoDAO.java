package br.com.gerenciadorvendas.modelo.dao;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.gerenciadorvendas.modelo.entidade.Produto;


/**
 * @author       $h@rk
 * @Date         09/11/2011
 * @project      GerenciadorVendasCore
 * @Description: 
 */
@Component
public class ProdutoDAO {

	private final EntityManager em;

	public ProdutoDAO(EntityManager em){
		this.em = em;
	}

	public int buscarTotalRegistros(){		

		String query = "select count(*) from Produto ";

		TypedQuery<Long>  q = this.em.createQuery(query, Long.class);

		return q.getSingleResult().intValue();	       
	}

	/**
	 * Realiza busca comecando pelo parametro inicio e com o maximo
	 * de elementos enviado no parametro maximo 
	 * @param primeiro
	 * @param maximo
	 * @return
	 */
	public List<Produto> buscarLimitadoPor(int inicio, int maximo) {

		List<Produto> list = Collections.emptyList();

		try {

			String hql = " from Produto c order by c.id";

			TypedQuery<Produto> query = this.em.createQuery(hql, Produto.class);

			query.setFirstResult(inicio);
			query.setMaxResults(maximo);

			list = query.getResultList();

		} catch (NoResultException e) {
			list = Collections.emptyList();
		}

		return list;
	}


	public Produto salvar(Produto produto){
		return this.em.merge(produto);
	}

	/**
	 * Retorna a Produto compativel com o id enviado
	 * 
	 * @param id
	 * @return
	 */
	public final Produto buscarPorID(Long id) {
		return this.em.find(Produto.class, id);
	}

	/**
	 * Buscar por nome
	 * @param nome
	 * @return
	 */
	public List<Produto> buscarPorNome(String nome) {

		String hql = " from Produto p where UPPER(TRIM(p.nome)) = :nome";

		TypedQuery<Produto> query = this.em.createQuery(hql, Produto.class);

		query.setParameter("nome", nome.toUpperCase().trim());

		return query.getResultList();
	}

	public void removerEmLote(List<Long> listId) {
		
		String hql = " delete from Produto p where p.id in :listId";

		Query query = this.em.createQuery(hql);

		query.setParameter("listId", listId);
		
		query.executeUpdate();
	}
}

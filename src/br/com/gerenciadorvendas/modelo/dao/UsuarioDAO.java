package br.com.gerenciadorvendas.modelo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.gerenciadorvendas.modelo.entidade.Usuario;


/**
 * @author       $h@rk
 * @Date         09/11/2011
 * @project      GerenciadorVendasCore
 * @Description: 
 */
@Component
public class UsuarioDAO {

	private final EntityManager em;

	public UsuarioDAO(EntityManager em){
		this.em = em;
	}

	/**
	 * Realiza busca por usuário e senha e retorna objeto encontrado
	 * @param nick
	 * @param senha
	 * @return
	 */
	public Usuario buscarPorNickSenha(String nick, String senha){
		
		String hql = " from Usuario u where u.nickName = :nick and u.senha = :senha";
		
		TypedQuery<Usuario> query = this.em.createQuery(hql, Usuario.class);
		
		query.setParameter("nick", nick);
		query.setParameter("senha", senha);
		
		List<Usuario> resultList = query.getResultList();
		
		return resultList.isEmpty() ? null : resultList.get(0);
	}

	public final Usuario buscarPorID(Long id) {
		return this.em.find(Usuario.class, id);
	}
}

package br.com.gerenciadorvendas.modelo.dao;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.ioc.Component;
import br.com.gerenciadorvendas.modelo.entidade.Unidade;

/**
 * Classe responsavel pelo acesso aos dados da unidade de medida
 * @author Thiago Ramalho
 *
 */
@Component
public class UnidadeDAO {

	private final EntityManager em;

	public UnidadeDAO(EntityManager em){
		this.em = em;
	}

	public int buscarTotalRegistros(){		

		String query = "select count(*) from Unidade ";

		TypedQuery<Long>  q = this.em.createQuery(query, Long.class);

		return q.getSingleResult().intValue();	       
	}

	/**
	 * Realiza busca comecando pelo parametro inicio e com o maximo
	 * de elementos enviado no parametro maximo 
	 * @param primeiro
	 * @param maximo
	 * @return
	 */
	public List<Unidade> buscarLimitadoPor(int inicio, int maximo) {

		List<Unidade> list = Collections.emptyList();

		try {

			String hql = " from Unidade c ";

			TypedQuery<Unidade> query = this.em.createQuery(hql, Unidade.class);

			query.setFirstResult(inicio);
			query.setMaxResults(maximo);

			list = query.getResultList();

		} catch (NoResultException e) {
			list = Collections.emptyList();
		}

		return list;
	}


	public void salvar(Unidade unidade){
		this.em.persist(unidade);
	}

	/**
	 * Retorna todas as Unidades cadastradas
	 * @return
	 */
	public List<Unidade> buscarTodas(){

		String hql = " from Unidade c ";
		TypedQuery<Unidade> query = this.em.createQuery(hql, Unidade.class);

		return query.getResultList();
	}

	/**
	 * Retorna a Unidade compativel com o id enviado
	 * 
	 * @param id
	 * @return
	 */
	public final Unidade buscarPorID(Long id) {
		return this.em.find(Unidade.class, id);
	}

	/**
	 * Buscar por nome
	 * @param sigla
	 * @return
	 */
	public List<Unidade> buscarPorSigla(String sigla) {

		String hql = " from Unidade c where UPPER(TRIM(c.sigla)) = :sigla";

		TypedQuery<Unidade> query = this.em.createQuery(hql, Unidade.class);

		query.setParameter("sigla", sigla.toUpperCase().trim());

		return query.getResultList();
	}
}

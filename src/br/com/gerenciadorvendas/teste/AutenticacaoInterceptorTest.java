package br.com.gerenciadorvendas.teste;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.DefaultResourceMethod;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.aplicacao.interceptadores.AutenticacaoInterceptor;
import br.com.gerenciadorvendas.aplicacao.interceptadores.InterceptorUtil;
import br.com.gerenciadorvendas.controle.IndexController;
import br.com.gerenciadorvendas.controle.LoginController;
import br.com.gerenciadorvendas.modelo.util.Publico;

public class AutenticacaoInterceptorTest {

	@Mock private SessaoAplicacao sessaoAplicacao;
	@Mock private Result result;
	@Mock private InterceptorStack stack;
	@Mock private LoginController loginController;
	@Mock private IndexController indexController;

	private InterceptorUtil interceptorUtil;
	private ResourceMethod method;
	private ResourceMethod method1;


	@Before
	public void setUp() throws Exception { 

		MockitoAnnotations.initMocks(this);

		method = DefaultResourceMethod.instanceFor(Resource.class, Resource.class.getMethod("method"));
        method1 = DefaultResourceMethod.instanceFor(Resource.class, Resource.class.getMethod("methodPublic"));

	}

	@Test
	public void deveRotearParaLogin(){

		Interceptor interceptor = new AutenticacaoInterceptor(result, sessaoAplicacao, interceptorUtil);

		when(sessaoAplicacao.isLogado()).thenReturn(false);
		when(result.redirectTo(LoginController.class)).thenReturn(loginController);

		interceptor.intercept(stack, method, loginController);

		verify(result).redirectTo(LoginController.class);
		verify(stack, never()).next(method, loginController);
	}

	@Test
	public void devePermitirRequisicao(){

		Interceptor interceptor = new AutenticacaoInterceptor(result, sessaoAplicacao, new InterceptorUtil());

		when(sessaoAplicacao.isLogado()).thenReturn(true);
		when(result.redirectTo(LoginController.class)).thenReturn(loginController);

		interceptor.intercept(stack, method, loginController);

		verify(result, never()).redirectTo(LoginController.class);
		verify(stack, times(1)).next(method, loginController);
	}

	@Test
	public void deveRotearParaIndexEstaLogadoTentandoAcessandoFormDeLogin(){

		Interceptor interceptor = new AutenticacaoInterceptor(result, sessaoAplicacao, new InterceptorUtil());

		when(sessaoAplicacao.isLogado()).thenReturn(true);
		when(result.redirectTo(LoginController.class)).thenReturn(loginController);
		when(result.redirectTo(IndexController.class)).thenReturn(indexController);

		interceptor.intercept(stack, method1, loginController);

		verify(result, never()).redirectTo(LoginController.class);
		verify(result, times(1)).redirectTo(IndexController.class);
	}


	@Publico 
	class ResourcePublic {
		public void method() {

		}
	}

	class Resource {
		public void method() {

		}

		@Publico
		public void methodPublic() {

		}
	}
}

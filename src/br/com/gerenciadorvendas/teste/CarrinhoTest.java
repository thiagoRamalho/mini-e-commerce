package br.com.gerenciadorvendas.teste;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import br.com.gerenciadorvendas.modelo.entidade.Carrinho;
import br.com.gerenciadorvendas.modelo.entidade.Item;
import br.com.gerenciadorvendas.modelo.entidade.Produto;
import br.com.gerenciadorvendas.modelo.entidade.Unidade;

public class CarrinhoTest {
	
	@Test
	public void deveCalcularOValorProdutosUnicos(){
		
		Carrinho carrinho = new Carrinho();
		
		//preco total =>  (3 * 2) = 6 
		Produto produto1 = this.criarProduto(3.0, 1, 2);
		//preco total =>  (1.5 * 3) = 4.5		
		Produto produto2 = this.criarProduto(1.5, 2, 3);
		
		carrinho.adicionar(produto1);
		carrinho.adicionar(produto2);
		
		BigDecimal obtido = carrinho.getPrecoTotal();
		
		BigDecimal esperado = new BigDecimal("10.5");
		esperado = esperado.setScale(obtido.scale());
		
		assertEquals(esperado, obtido);
		
	}
	
	@Test
	public void deveCalcularValorProdutoRepetido(){
		
		Carrinho carrinho = new Carrinho();
		
		//preco total =>  (3 * 2) = 6 
		Produto produto1 = this.criarProduto(3.0, 1, 2);
		//preco total =>  (1.5 * 3) = 4.5		
		Produto produto2 = this.criarProduto(1.5, 2, 3);
		//preco total =>  (1.5 * 3) = 4.5		
		Produto produto3 = this.criarProduto(1.5, 2, 3);
		
		carrinho.adicionar(produto1);
		carrinho.adicionar(produto2);
		carrinho.adicionar(produto3);
		
		BigDecimal obtido = carrinho.getPrecoTotal();
		
		BigDecimal esperado = new BigDecimal("15");
		esperado = esperado.setScale(obtido.scale());
		
		assertEquals(esperado, obtido);
		
	}
	
	@Test
	public void removerItem(){
		
		Carrinho carrinho = new Carrinho();
		
		Produto produto1 = this.criarProduto(10.0, 1, 2);
		
		//adiciona duas vezes para simular duas adicoes no carrinho
		carrinho.adicionar(produto1);
		carrinho.adicionar(produto1);
		
		Produto produto2 = this.criarProduto(15.0, 2, 2);
		
		carrinho.adicionar(produto2);
		
		BigDecimal obtido = carrinho.getPrecoTotal();
		
		BigDecimal esperado = new BigDecimal("70");
		esperado = esperado.setScale(obtido.scale());
		
		assertEquals(esperado, obtido);
		
		Item itemRemocao = new Item(produto1);
		//forca a quantidade para simular remocao
		itemRemocao.setQuantidadeSolicitada(0);
		
		carrinho.atualizar(itemRemocao);
		
		obtido = carrinho.getPrecoTotal();
		
		esperado = new BigDecimal("30");
		esperado = esperado.setScale(obtido.scale());

		//deve possuir o preco apenas do produto2
		assertEquals(esperado, obtido);
	}
	
	@Test
	public void atualizarItemComQuantidadeInferiorAoExistente(){
		
		Carrinho carrinho = new Carrinho();
		
		Produto produto1 = this.criarProduto(10.0, 1, 2);
		
		//adiciona duas vezes para simular duas adicoes no carrinho
		carrinho.adicionar(produto1);
		carrinho.adicionar(produto1);
		
		Produto produto2 = this.criarProduto(15.0, 2, 2);
		
		carrinho.adicionar(produto2);
		
		BigDecimal obtido = carrinho.getPrecoTotal();
		
		BigDecimal esperado = new BigDecimal("70");
		esperado = esperado.setScale(obtido.scale());
		
		assertEquals(esperado, obtido);
		
		Item itemRemocao = new Item(produto1);
		
		//simula que foi diminuida a quantidade ja que o
		//produto deste item foi adicionado duas vezes
		itemRemocao.setQuantidadeSolicitada(1);
		
		carrinho.atualizar(itemRemocao);
		
		obtido = carrinho.getPrecoTotal();
		
		esperado = new BigDecimal("50");
		esperado = esperado.setScale(obtido.scale());

		assertEquals(esperado, obtido);
	}
	
	@Test
	public void atualizarItemComQuantidadeSuperiorAoExistente(){
		
		Carrinho carrinho = new Carrinho();
		
		Produto produto1 = this.criarProduto(10.0, 1, 2);
		
		//adiciona duas vezes para simular duas adicoes no carrinho
		carrinho.adicionar(produto1);
		carrinho.adicionar(produto1);
		
		Produto produto2 = this.criarProduto(15.0, 2, 2);
		
		carrinho.adicionar(produto2);
		
		BigDecimal obtido = carrinho.getPrecoTotal();
		
		BigDecimal esperado = new BigDecimal("70");
		esperado = esperado.setScale(obtido.scale());
		
		assertEquals(esperado, obtido);
		
		Item itemRemocao = new Item(produto1);
		
		//simula que foi diminuida a quantidade ja que o
		//produto deste item foi adicionado duas vezes
		itemRemocao.setQuantidadeSolicitada(3);
		
		carrinho.atualizar(itemRemocao);
		
		obtido = carrinho.getPrecoTotal();
		
		esperado = new BigDecimal("90");
		esperado = esperado.setScale(obtido.scale());

		assertEquals(esperado, obtido);
	}
	
	@Test
	public void manterValorQuantidadeIdenticaAExistente(){
		
		Carrinho carrinho = new Carrinho();
		
		Produto produto1 = this.criarProduto(10.0, 1, 2);
		
		//adiciona duas vezes para simular duas adicoes no carrinho
		carrinho.adicionar(produto1);
		carrinho.adicionar(produto1);
		
		Produto produto2 = this.criarProduto(15.0, 2, 2);
		
		carrinho.adicionar(produto2);
		
		BigDecimal obtido = carrinho.getPrecoTotal();
		
		BigDecimal esperado = new BigDecimal("70");
		esperado = esperado.setScale(obtido.scale());
		
		assertEquals(esperado, obtido);
		
		Item itemRemocao = new Item(produto1);
		
		//mandando a mesma quantidade ja existente
		itemRemocao.setQuantidadeSolicitada(2);
		
		carrinho.atualizar(itemRemocao);
		
		obtido = carrinho.getPrecoTotal();
		
		esperado = new BigDecimal("70");
		esperado = esperado.setScale(obtido.scale());

		assertEquals(esperado, obtido);
	}
	
	/**
	 * Fabrica de produtos com informacoes essenciais para
	 * o teste
	 * 
	 * @param preco
	 * @param idProduto
	 * @return
	 */
	private Produto criarProduto(double preco, int idProduto, int quantidadePorEmbalagem){
		
		Unidade unidade = new Unidade();
		unidade.setQuantidade(quantidadePorEmbalagem);
		unidade.setSigla("CX");
		
		Produto produto = new Produto();
		produto.setId(new Long(idProduto));
		produto.setPreco(new BigDecimal(""+preco));
		produto.setUnidade(unidade);
		
		return produto;
	}

}

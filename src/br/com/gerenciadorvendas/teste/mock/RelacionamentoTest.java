package br.com.gerenciadorvendas.teste.mock;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;

public class RelacionamentoTest {/*

	public static void main(String[] args) {		

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("teste");
		EntityManager entityManager = factory.createEntityManager();

		try{

			entityManager.getTransaction().begin();
			
			Prod prod = new Prod(10);
			
			Nota nota1 = new Nota();
			nota1.adicionarItem(new Item(2, prod, nota1));

			Nota nota2 = new Nota();
			nota2.adicionarItem(new Item(2, prod, nota2));
			
			entityManager.persist(prod);
			entityManager.persist(nota1);
			entityManager.persist(nota2);
			
			entityManager.getTransaction().commit();
			
			List<Item> itens = nota2.getItens();
			
			for (Item item : itens) {
				System.out.println(item.getId() +" "+item.getPreco());
			}

			
			entityManager.close();

			factory.close();

		}catch(Exception e){
			e.printStackTrace();
			entityManager.getTransaction().rollback();

		}
	}
*/}

@Entity
class Prod{
	@Id @GeneratedValue
	private Long id;
	
	private double preco;
	
	Prod(){}
	
	public Prod(double preco){
		this.preco = preco;
	}
	
	public double getPreco(){
		return this.preco;
	}
}

@Entity
class Item{

	@Id @GeneratedValue
	private Long id;
	private int quantidade;
	
	private double preco;
	
	@OneToOne
	private Prod prod;
	
	@ManyToOne(optional = false)
	private Nota nota;
	
   Item(){}

	Item(int quantidade, Prod prod){
		this.quantidade = quantidade;
		this.prod = prod;
		this.getPreco();
	}

   
	Item(int quantidade, Prod prod, Nota nota){
		this.quantidade = quantidade;
		this.prod = prod;
		this.nota = nota;
		this.getPreco();
	}
	
	public int getQuantidade(){
		return this.quantidade;
	}
	
	public double getPreco(){
		this.preco = this.prod.getPreco() * this.quantidade;
		return this.preco;
	}
	
	public Long getId(){
		return this.id;
	}
}


@Entity
class Nota{
	
	@Id @GeneratedValue
	private Long id;
	
	private double totalNota;
	
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy="nota")
	private List<Item> item = new ArrayList<Item>();
	
	public void adicionarItem(Item item){
		this.item.add(item);
		this.totalNota+= item.getPreco();
	}
	
	public List<Item> getItens(){
		return this.item;
	}
	
	public double getTotalNota(){
		return this.totalNota;
	}
	
	public Long getId(){
		return this.id;
	}
}
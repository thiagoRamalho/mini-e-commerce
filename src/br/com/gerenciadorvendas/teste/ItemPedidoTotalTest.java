package br.com.gerenciadorvendas.teste;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import br.com.gerenciadorvendas.modelo.entidade.Item;
import br.com.gerenciadorvendas.modelo.entidade.Produto;
import br.com.gerenciadorvendas.modelo.entidade.Unidade;

public class ItemPedidoTotalTest {

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void deveCalcularTotalEmbalagemCom_10Unid_e_2_SolicitacoesProduto(){
		
		Item itemPedido = this.criarItemPedido(new BigDecimal("10"), 10);
		
		itemPedido.setQuantidadeSolicitada(2);
		
		BigDecimal valor = itemPedido.getPrecoTotal();
		
		assertEquals(new BigDecimal("200").intValue(), valor.intValue());
	}
	
	/**
	 * Cria um item de Pedido contendo pedido com os valores enviados 
	 * 
	 * @param precoProduto        - Preco unitario do produto
	 * @param quantidadeEmbalagem - Quantidade de Produtos por embalagem
	 * @return
	 */
	private Item criarItemPedido(BigDecimal precoProduto, int quantidadeEmbalagem){
		
		Produto p = new Produto();
		p.setPreco(precoProduto);
		
		Unidade u = new Unidade();
		u.setQuantidade(quantidadeEmbalagem);
		
		p.setUnidade(u);
		
		return new Item(p);
	}
}

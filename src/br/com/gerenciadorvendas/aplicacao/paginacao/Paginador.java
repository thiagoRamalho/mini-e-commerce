package br.com.gerenciadorvendas.aplicacao.paginacao;


/**
 * Classe responsavel por realizar os calculos e determinar
 * o numero de elementos de paginacao
 * @author Thiago Ramalho
 *
 */
public class Paginador {

	private int totalPaginas;
	private int maximoPorPagina;
	private int paginaAtual;
	private int inicioPesquisa;

	/**
	 * Realiza os calculos para determinar o valor da proxima pagina e total de 
	 * paginas que devem ser criadas
	 * 
	 * @param paginaAtual
	 * @param maximoPorPagina
	 * @param totalRegistros
	 */
	public void calcular(Integer paginaAtual, int maximoPorPagina, int totalRegistros) {

		this.maximoPorPagina = maximoPorPagina;
		
		//tratamento para o caso de ainda nao estar acessando uma pagina
		this.paginaAtual = paginaAtual == null ? 1 : paginaAtual.intValue();

		//valor inicial do range de registros
		this.inicioPesquisa = (this.maximoPorPagina * this.paginaAtual) - this.maximoPorPagina;

		//total de paginas que devem ser criadas
		this.totalPaginas = new Double(Math.ceil((double)totalRegistros / this.maximoPorPagina)).intValue();
	}
	
	public int getInicioPesquisa() {
		return inicioPesquisa;
	}

	public int getTotalPaginas() {
		return totalPaginas;
	}

	public int getMaximoPorPagina() {
		return maximoPorPagina;
	}

	public int getPaginaAtual() {
		return paginaAtual;
	}
}

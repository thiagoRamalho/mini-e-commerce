package br.com.gerenciadorvendas.aplicacao.validadores;

import br.com.caelum.vraptor.validator.Validations;

/**
 * 
 * @author Thiago Ramalho
 * Verifica objetos nulos
 *
 */
public class NuloValidator extends Validations {

	public NuloValidator(Object elemento) {
		this.verificar(elemento);
	}

	/**
	 * Lanca erro caso objeto esteja nulo
	 * @param usuario
	 */
	private void verificar(Object elemento) {
		that(elemento != null, "error", "operacao.erro");
	}
}

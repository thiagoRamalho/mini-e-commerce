package br.com.gerenciadorvendas.aplicacao.validadores;


import br.com.caelum.vraptor.validator.Validations;
import br.com.gerenciadorvendas.modelo.entidade.Entidade;

/**
 * 
 * @author Thiago Ramalho
 * Valida se as entidades sao o mesmo elemento validando pelo seu id
 *
 */
public class NomeDuplicadoValidator extends Validations {

	public NomeDuplicadoValidator(Entidade pesquisa, Entidade entrada) {
		this.verificar(pesquisa, entrada);
	}

	/**
	 * Compara os id's para definir se essa e uma situacao de duplicidade
	 * @param usuario
	 */
	private void verificar(Entidade pesquisa, Entidade entrada) {
		
		//se a entidade foi encontrada
		if(pesquisa != null){
			//se possui o mesmo id nao existe erro, do contrario e duplicidade
			that((pesquisa.getId().equals(entrada.getId())), "error", "duplicidade");
		}
		
		
	}
}

package br.com.gerenciadorvendas.aplicacao.validadores;


import br.com.caelum.vraptor.validator.Validations;
import br.com.gerenciadorvendas.modelo.entidade.Usuario;

/**
 * 
 * @author Thiago Ramalho
 * Classe responsavel por verificar se um determinado objeto e valido
 * ou seja, se possui identidade
 *
 */
public class LoginValidator extends Validations {

	public LoginValidator(Usuario usuario) {
		this.verificar(usuario);
	}

	/**
	 * Verifica se usuário é valido
	 * @param usuario
	 */
	private void verificar(Usuario usuario) {
		that((usuario != null && usuario.getId() != null), "error", "login.invalido");
	}
}

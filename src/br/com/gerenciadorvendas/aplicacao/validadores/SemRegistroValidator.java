package br.com.gerenciadorvendas.aplicacao.validadores;


import java.util.List;

import br.com.caelum.vraptor.validator.Validations;
import br.com.gerenciadorvendas.modelo.entidade.Entidade;

/**
 * 
 * @author Thiago Ramalho
 * Valida se as entidades possuem cadastro
 *
 */
public class SemRegistroValidator extends Validations {

	public SemRegistroValidator(List<? extends Entidade> entidades, Class<? extends Entidade> clazz) {
		this.verificar(entidades, clazz);
	}

	/**
	 * Compara os id's para definir se essa e uma situacao de duplicidade
	 * @param usuario
	 */
	private void verificar(List<? extends Entidade> entidades, Class<? extends Entidade> clazz) {
		
			//se nao possuir valor na lista entao lanca mensagem de erro
			that(!entidades.isEmpty(), "error", "obrigatorio.entidade", clazz.getSimpleName().toLowerCase());
	}
}

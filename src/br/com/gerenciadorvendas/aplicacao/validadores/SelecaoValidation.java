package br.com.gerenciadorvendas.aplicacao.validadores;

import java.util.List;

import br.com.caelum.vraptor.validator.Validations;

/**
 * Classe responsavel por verificar se a selecao e unica ou multipla
 * @author Thiago Ramalho
 *
 */
public class SelecaoValidation extends Validations {

	private final String ERRO = "error";

	public SelecaoValidation(List<Long> listId, boolean isSelecaoUnica) {
		this.verificar(listId, isSelecaoUnica);
	}

	/**
	 * Verifica os tipos de selecao validando o tamanho da lista de elementos
	 * @param listId
	 * @param isSelecaoUnica
	 */
	private void verificar(List<Long> listId, boolean isSelecaoUnica) {
		
		//verifica se houve selecao...
		if(that(listId != null && listId.size() > 0, ERRO,"selecao.invalida")){

			//caso permita apenas selecao unica...
			if(isSelecaoUnica){
				
				//verifica se houve selecao de apenas um registro
				that(listId.size() == 1, ERRO, "multipla.selecao.invalida");
			}
		}
	}

}

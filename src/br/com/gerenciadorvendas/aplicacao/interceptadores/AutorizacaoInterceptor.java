package br.com.gerenciadorvendas.aplicacao.interceptadores;

import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.controle.IndexController;
import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;
import br.com.gerenciadorvendas.modelo.util.Permissao;
import br.com.gerenciadorvendas.modelo.util.Publico;

/*** 
 * @author Thiago Ramalho
 * 
 * Classe responsavel por determinar se o usuario tem permissao
 * de acesso a determinados recursos da aplicacao
 * 
 * Obs.: Sera executado apenas apos do interceptator de autenticacao
 */
@Intercepts(after = AutenticacaoInterceptor.class)
public class AutorizacaoInterceptor implements Interceptor{

	private Result result;
	private SessaoAplicacao sessaoAplicacao;
	
	private static final Logger logger = Logger.getLogger(AutorizacaoInterceptor.class);

	
	public AutorizacaoInterceptor(Result result, SessaoAplicacao sessaoAplicacao) {
		super();
		this.result = result;
		this.sessaoAplicacao = sessaoAplicacao;
	}

	/**
	 * Se nao contiver anotacao publica em metodo ou classe
	 * entao deve interceptar o recurso
	 */
	@Override
	public boolean accepts(ResourceMethod method) {
		
		logger.debug("verificando se recurso sera interceptado");
		
		return !(
				method.getMethod().isAnnotationPresent(Publico.class) || 
				method.getResource().getType().isAnnotationPresent(Publico.class));
	}

	/**
	 * Com base no recurso que esta tendo ser acessado verifica suas anotacoes e compara
	 * com o nivel de acesso do usuario, permitindo ou negando o acesso
	 */
	@Override
	public void intercept(InterceptorStack stack, ResourceMethod method, Object resource) {
	    
		Permissao methodPermissao = method.getMethod().getAnnotation(Permissao.class);
	    Permissao controllerPermissao = method.getResource().getType().getAnnotation(Permissao.class);

	    logger.debug("interceptou recurso");
	    
	    if (this.possuiNivelAcesso(methodPermissao) && this.possuiNivelAcesso(controllerPermissao)) {
	        logger.debug("permite acesso ao recurso, continuando a requisicao");
	    	stack.next(method, resource);
	    } else {
	        logger.debug("acesso negado, redirecionando para o index");
	        result.include("message", "sem.direito.acesso");
	        result.redirectTo(IndexController.class).index();
	    }
	}
	
	/**
	 * Compara o perfil do usuario com o nivel de acesso e retorna booleano
	 * definindo se o usuario pode ou nao acessar o recurso
	 * @param permissao
	 * @return
	 */
	private boolean possuiNivelAcesso(Permissao permissao) {
		
	    //se nao possui anotacao de nivel assume que o acesso e permitido
		if (permissao == null) {
	       return true;
	    }
		
		if(!this.sessaoAplicacao.isLogado()){
			return false;
		}
		
	    Collection<PerfilUsuario> perfilList = Arrays.asList(permissao.value());
	    
	    PerfilUsuario perfilUsuario = this.sessaoAplicacao.getUsuario().getPerfilUsuario();
	    
	    logger.debug("perfil usuario: "+(perfilUsuario == null ? "null" : perfilUsuario.name()));
	    
	    //verifica se o perfil do usuario esta presente nos niveis do recurso
	    return perfilList.contains(perfilUsuario);
	}
}

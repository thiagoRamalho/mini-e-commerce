package br.com.gerenciadorvendas.aplicacao.interceptadores;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.gerenciadorvendas.modelo.util.Publico;

/**
 * Classe com metodos utilitarios utilizados pelos interceptadores
 * esta em escopo de aplicacao pois precisamos de apenas uma para
 * todo contexto de execucao (funciona como singleton) 
 * @author Thiago Ramalho
 *
 */
@Component
@ApplicationScoped
public class InterceptorUtil {

	/**
	 * Verifica se o recurso em execucao possuia a anotacao publica
	 * @param method
	 * @return
	 */
	 boolean isAnotadoComoPublico(ResourceMethod method){
		//verifica se a lógica executada está anotada como pública
		return
		 (method.getMethod().isAnnotationPresent(Publico.class) ||
		  method.getResource().getType().isAnnotationPresent(Publico.class));	
	}

}

package br.com.gerenciadorvendas.aplicacao.interceptadores;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.controle.IndexController;
import br.com.gerenciadorvendas.controle.LoginController;

/**
 * 
 * @author Thiago Ramalho
 * 
 * Classe responsavel por verificar se usuario esta autenticado na aplicacao
 * antes de executar acoes
 *
 */
@Intercepts
public class AutenticacaoInterceptor implements Interceptor{

	protected Result result;
	private SessaoAplicacao session;

	private static final Logger logger = Logger.getLogger(AutenticacaoInterceptor.class);
	private InterceptorUtil interceptorUtil;

	public AutenticacaoInterceptor(Result result, SessaoAplicacao session, InterceptorUtil interceptorUtil) {
		this.result = result;
		this.session = session;
		this.interceptorUtil = interceptorUtil;
	}
	
	/**
	 * Verifica se deve interceptar a lógica em execução
	 */
	public boolean accepts(ResourceMethod method) {
			return (!this.interceptorUtil.isAnotadoComoPublico(method) || this.session.isLogado());
		}
	
	/**
	 * Verifica se deve redirecionar a execução com base nos controles de acesso
	 */
	public void intercept(InterceptorStack stack, ResourceMethod method, 
			Object resourceInstance)throws InterceptionException {

		logger.debug("interceptou chamada");
		
		//define a estratégia verificando se usuário está logado
		Interceptor interceptor =  this.session.isLogado() ? 
				new LogadoStrategy() : new NaoLogadoStrategy();
				
		interceptor.intercept(stack, method, resourceInstance);
	}
	
	// implementações de estratégias
	
	/**
	 * Esratégia que assume usuário logado
	 * @author Thiago Ramalho
	 *
	 */
	class LogadoStrategy implements Interceptor{

		@Override
		public boolean accepts(ResourceMethod arg0) {
			return true;
		}

		@Override
		public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {
			
			logger.debug("usuario esta logado");

			//vericamos se o mesmo est? tentando acessar a p?gina de
			//login
			if(interceptorUtil.isAnotadoComoPublico(method)){

				logger.debug("esta tentando acessar o login");
				logger.debug("redirecionando para o index");

				//em caso positivo redirecionamos para a home
				result.redirectTo(IndexController.class).index();

			}else{

				logger.debug("permite a execucao da logica");

				//se não está tentando acessar o login, 
				//deixamos a execução ocorrer
				stack.next(method, resourceInstance);
			}

		}
	}
	
	/**
	 * Classe que implementa a estratégia quando
	 * usuário não está logado
	 * 
	 * @author Thiago Ramalho
	 *
	 */
	class NaoLogadoStrategy implements Interceptor{

		@Override
		public boolean accepts(ResourceMethod arg0) {
			return true;
		}

		@Override
		public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {
			//não está logado, redireciona para o login
			logger.debug("Nao esta logado");
			logger.debug("redirecionando para o login");
			result.redirectTo(LoginController.class).login();
		}
	}
}

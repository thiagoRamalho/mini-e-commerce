package br.com.gerenciadorvendas.aplicacao;

import java.util.Collection;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.gerenciadorvendas.modelo.entidade.Item;
import br.com.gerenciadorvendas.modelo.entidade.Carrinho;
import br.com.gerenciadorvendas.modelo.entidade.Produto;
import br.com.gerenciadorvendas.modelo.entidade.Usuario;
import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;

/**
 * 
 * @author Thiago Ramalho
 * Classe responsavel por manter a sessao do usuario 
 * e suas informacoes relevantes para aplicacao
 */
@Component
@SessionScoped
public class SessaoAplicacao {

	private final int MAXIMO_PAGINA_PADRAO = 5;
	private Usuario usuario;
	private int maximoPorPagina;
	private Carrinho carrinho;

	public SessaoAplicacao(){
		reiniciar();
	}

	/**
	 * Adiciona Produto ao carrinho
	 * @param produto
	 */
	public Carrinho getCarrinho(){
		return this.carrinho;
	}
	
	public int getQuantidadeItensCarrinho(){
		return this.carrinho.getQuantidadeItens();
	}
	
	/**
	 * Reinicializa sessao
	 */
	private void reiniciar() {
		this.carrinho = new Carrinho();
		this.usuario = new Usuario();
		this.setMaximoPorPagina(this.getMaximoPorPagina() == 0 ? MAXIMO_PAGINA_PADRAO : this.getMaximoPorPagina());
	}

	/**
	 * Verifica a existencia de usuario na sessao, para isso
	 * considera que o objeto existe e que possui id
	 * @return
	 */
	public boolean isLogado(){
		return this.usuario != null && this.usuario.getId() != null;
	}

	/**
	 * Retorna o usuario caso exista ou nulo
	 * @return
	 */
	public Usuario getUsuario(){
		return this.usuario;
	}

	/**
	 * Finaliza a sessao do usuario
	 */
	public void logout() {
		this.reiniciar();
	}

	/**
	 * Adiciona usuario a sessao
	 * @param usuario
	 */
	public void setUsuario(Usuario usuario) {
		if(usuario != null){
			this.usuario = usuario.clone();
		}
	}
	
	public boolean isCliente(){
		return isLogado() ? 
				this.usuario.getPerfilUsuario().equals(PerfilUsuario.CLIENTE) : false;
	}

	public int getMaximoPorPagina() {
		return maximoPorPagina;
	}

	/**
	 * Seta o maximo de retorno de elementos por pagina
	 * ou o valor padrao caso o parametro seja zero
	 * 
	 * @param maximoPorPagina
	 */
	public void setMaximoPorPagina(int maximoPorPagina) {
		
		if(maximoPorPagina == 0){
			maximoPorPagina = MAXIMO_PAGINA_PADRAO;
		}
		
		this.maximoPorPagina = maximoPorPagina;
	}
}

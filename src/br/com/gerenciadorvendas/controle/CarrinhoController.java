package br.com.gerenciadorvendas.controle;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.modelo.dao.ProdutoDAO;
import br.com.gerenciadorvendas.modelo.entidade.Carrinho;
import br.com.gerenciadorvendas.modelo.entidade.Item;
import br.com.gerenciadorvendas.modelo.entidade.Produto;
import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;
import br.com.gerenciadorvendas.modelo.util.Permissao;

/**
 * Classe responsavel por controlar o acesso aos pedidos
 * @author Thiago Ramalho
 *
 */
@Permissao(PerfilUsuario.CLIENTE)
@Resource
public class CarrinhoController {

	private  final String CARRINHO = "/carrinho";
	private  final String CARRINHO_ADICIONAR = CARRINHO+"/adicionar/{produto.id}";

	private SessaoAplicacao sessaoAplicacao;
	private Result result;
	private ProdutoDAO produtoDAO;
	
	static final Logger logger = Logger.getLogger(CarrinhoController.class);
	
	public CarrinhoController(ProdutoDAO produtoDAO,SessaoAplicacao sessaoAplicacao, Result result) {
		super();
		this.produtoDAO = produtoDAO;
		this.sessaoAplicacao = sessaoAplicacao;
		this.result = result;
	}


	/**
	 * Adiciona um novo produto ao carrinho de compras
	 * @param produto
	 */
	@Post(CARRINHO_ADICIONAR)
	public void adicionarCarrinho(Produto produto){
		
		produto = this.produtoDAO.buscarPorID(produto.getId());
		
		logger.info("adicionou o produto: "+(produto.getNome())+" ao carrinho");
		
		Carrinho carrinho = this.sessaoAplicacao.getCarrinho();
		
		carrinho.adicionar(produto);
		
		this.result.redirectTo(this).carrinho();
	}
	
	/**
	 * Remove o item do carrinho
	 * 
	 * @param produto
	 */
	@Put(CARRINHO)
	public void atualizarQuantidade(List<Item> itens){
		
		logger.info("atualizando carrinho");
		
		Carrinho carrinho = this.sessaoAplicacao.getCarrinho();
		
		for (Item item : itens) {
			logger.debug("atualizando item quantidade: "+(item.getQuantidadeSolicitada())+" | item.idProduto: "+
						item.getIdProduto());
			carrinho.atualizar(item);
		}
		
		this.result.redirectTo(this).carrinho();
	}
	
	/**
	 * Remove todos os itens do carrinho
	 * 
	 * @param produto
	 */
	@Delete(CARRINHO)
	public void limpar(){
		
		Carrinho carrinho = this.sessaoAplicacao.getCarrinho();
		
		logger.debug("limpando o carrinho");
		
		carrinho.limpar();
		
		this.result.redirectTo(this).carrinho();
	}

	
	/**
	 * Redireciona para o carrinho de compras
	 */
	@Get(CARRINHO)
	public Carrinho carrinho() {
		
		logger.info("acessando carrinho");
		
		return this.sessaoAplicacao.getCarrinho();
	}
	
}

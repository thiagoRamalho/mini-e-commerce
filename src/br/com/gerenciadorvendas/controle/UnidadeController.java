package br.com.gerenciadorvendas.controle;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.aplicacao.paginacao.Paginador;
import br.com.gerenciadorvendas.aplicacao.validadores.NomeDuplicadoValidator;
import br.com.gerenciadorvendas.aplicacao.validadores.SelecaoValidation;
import br.com.gerenciadorvendas.modelo.dao.UnidadeDAO;
import br.com.gerenciadorvendas.modelo.entidade.Unidade;
import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;
import br.com.gerenciadorvendas.modelo.util.Permissao;

/**
 * 
 * @author Thiago Ramalho
 * Classe responsavel por controlar os acessos as unidades de medida
 */
@Permissao(PerfilUsuario.FORNECEDOR)
@Resource
public class UnidadeController {

	private static final String UNIDADE = "/unidade";
	private static final String UNIDADE_LISTAR = UNIDADE+"/listar";
	private static final String UNIDADE_LISTAR_PAGINA = UNIDADE_LISTAR+"/{pagina}";
	private static final String UNIDADE_ESPECIFICA = UNIDADE+"/{unidade.id}";
	
	private Result result;
	private Validator validator;

	private UnidadeDAO unidadeDAO;
	private SessaoAplicacao sessaoAplicacao;
	private Paginador paginador;

	static final Logger logger = Logger.getLogger(UnidadeController.class);


	public UnidadeController(Result result, Validator validator, UnidadeDAO unidadeDAO, SessaoAplicacao sessaoAplicacao, Paginador paginador) {
		this.result = result;
		this.validator = validator;
		this.unidadeDAO = unidadeDAO;
		this.sessaoAplicacao = sessaoAplicacao;
		this.paginador = paginador;
	}

	@Get(UNIDADE)
	public void formulario(){}

	/**
	 * Lista as entidades cadastradas
	 * @param pagina
	 */
	@Get({UNIDADE_LISTAR_PAGINA, UNIDADE_LISTAR})
	public void listar(Integer pagina){

		logger.info(" * pagina selecionada: "+(pagina));

		int totalRegistros = this.unidadeDAO.buscarTotalRegistros();

		//realiza o calcula para determinar o valor do range de pesquisa
		paginador.calcular(pagina, this.sessaoAplicacao.getMaximoPorPagina(), totalRegistros);

		//pesquisa pelo range encontrado
		List<Unidade> lista = 
				this.unidadeDAO.buscarLimitadoPor(paginador.getInicioPesquisa(), paginador.getMaximoPorPagina());

		if(lista.isEmpty()){

			logger.debug(" Nao encontrou registros");

			//informa sobre resultado da operacao
			this.result.include("message","sem.registros");

			//redireciona para pagina inicial
			this.result.redirectTo(IndexController.class).index();

		} else {

			logger.debug("enviando lista de registros para a requisicao ");

			//adiciona as entidades na requisicao
			this.result.include("unidadeList", lista);
			this.result.include("paginador", this.paginador);
		}		
	}	

	/**
	 * Salva a entidade
	 * 
	 * @param unidade
	 */
	@Post(UNIDADE)
	public void cadastrar(Unidade unidade){

		logger.info("cadastrar");
		
		this.validacaoIntegridade(unidade);
		
		this.validacaoDuplicidade(unidade);
		
		unidade.setDataAtualizacao(Calendar.getInstance());
		unidade.setDataCadastro(Calendar.getInstance());

		this.persistencia(unidade);
	}
	
	/**
	 * Atualiza objeto 
	 * @param id
	 */
	@Put(UNIDADE_ESPECIFICA)
	public void atualizar(Unidade unidade){
		
		logger.info("atualizar");

		validacaoIntegridade(unidade);

		validacaoDuplicidade(unidade);
		
		//realiza pesquisa para tornar managed
		Unidade managed = this.unidadeDAO.buscarPorID(unidade.getId());
		
		//atualizamos as informacoes no objeto managed para evitar erros do ORM
		managed.setSigla(unidade.getSigla());
		managed.setQuantidade(unidade.getQuantidade());
		managed.setDescricao(unidade.getDescricao());
		managed.setDataAtualizacao(Calendar.getInstance());

		this.persistencia(managed);
	}	
	
	/**
	 * Prepara objeto para visualizacao
	 * 
	 * @param listId
	 */
	@Put(UNIDADE)
	public void prepararParaEdicao(List<Long> listId){

		logger.debug("prepararParaEdicao");
		
		//realiza regra de selecao de elementos
		this.validator.checking(new SelecaoValidation(listId, true));

		logger.debug("validou selecao");

		//caso ocorram erros, framework desvia o fluxo para a lista
		//exibindo mensagens de erro
		validator.onErrorUsePageOf(this).listar(1);

		Long id = listId.get(0);

		logger.debug("ID pesquisado: "+(id));

		//busca a entidade pelo seu id
		Unidade unidade = this.unidadeDAO.buscarPorID(id);

		//se encontrar objeto...
		if(unidade != null){

			//setamos valor para view
			this.result.include("unidade", unidade);

			logger.debug("encontrou entidade pelo ID");

			//redireciona para a p·gina de ediÁ„o
			this.result.forwardTo(this).formulario();

		} else {

			logger.debug("registro nao encontrado");

			//informa sobre resultado da operacao
			this.result.include("message","operacao.erro");

			this.result.redirectTo(this).listar(1);
		}
	
	}

	/**
	 * Cadastra ou atualiza objeto
	 * @param unidade
	 */
	private void persistencia(Unidade unidade) {
		
		logger.info("ira salvar o objeto: "+(unidade));
		
		this.unidadeDAO.salvar(unidade);

		this.result.include("message", "operacao.sucesso");

		//redireciona para o painel do usuário
		this.result.redirectTo(this).listar(1);
	}

	/**
	 * Verifica se as informacoes do objeto ja foram cadastradas
	 * @param unidade
	 * @param managed
	 */
	private void validacaoDuplicidade(Unidade unidade) {
		
		//realiza pesquisa para validar duplicidade
		List<Unidade> lista = this.unidadeDAO.buscarPorSigla(unidade.getSigla());

		//se encontrou retiramos o registro da lista para validacao
		Unidade managed = lista.isEmpty() ? null : lista.get(0);
		
		//checa retorno da consulta 
		this.validator.checking(new NomeDuplicadoValidator(managed, unidade));

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro criada pelo validator
		validator.onErrorUsePageOf(this).formulario();

		logger.info("realizou verificacao de duplicidade");
	}

	/**
	 * Verifica se possui as informacoes obrigatorias
	 * @param unidade
	 */
	private void validacaoIntegridade(Unidade unidade) {
		
		logger.debug(" Id:"+unidade.getId()+"  Nome: "+unidade.getSigla()+" | Descricao: "+unidade.getDescricao());

		this.validator.validate(unidade);

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro
		validator.onErrorUsePageOf(this).formulario();

		logger.info("realizou validacao de campos");
	}
}

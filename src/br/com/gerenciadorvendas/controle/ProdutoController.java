package br.com.gerenciadorvendas.controle;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.aplicacao.paginacao.Paginador;
import br.com.gerenciadorvendas.aplicacao.validadores.NomeDuplicadoValidator;
import br.com.gerenciadorvendas.aplicacao.validadores.NuloValidator;
import br.com.gerenciadorvendas.aplicacao.validadores.SelecaoValidation;
import br.com.gerenciadorvendas.aplicacao.validadores.SemRegistroValidator;
import br.com.gerenciadorvendas.modelo.dao.CategoriaDAO;
import br.com.gerenciadorvendas.modelo.dao.ProdutoDAO;
import br.com.gerenciadorvendas.modelo.dao.UnidadeDAO;
import br.com.gerenciadorvendas.modelo.entidade.Categoria;
import br.com.gerenciadorvendas.modelo.entidade.Entidade;
import br.com.gerenciadorvendas.modelo.entidade.Produto;
import br.com.gerenciadorvendas.modelo.entidade.Unidade;
import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;
import br.com.gerenciadorvendas.modelo.util.Permissao;

/**
 * Classe responsavel por controlar o acesso a entidade produto
 * @author Thiago Ramalho
 *
 */
@Resource
public class ProdutoController {

	private static final String PRODUTO = "/produto";
	private static final String PRODUTO_LISTAR = PRODUTO+"/listar";
	private static final String PRODUTO_LISTAR_PAGINA = PRODUTO_LISTAR+"/{pagina}";
	private static final String PRODUTO_ESPECIFICO = PRODUTO+"/{id}";
	
	private Result result;
	private Validator validator;

	private CategoriaDAO categoriaDAO;
	private UnidadeDAO unidadeDAO;
	private ProdutoDAO produtoDAO;
	
	private SessaoAplicacao sessaoAplicacao;
	private Paginador paginador;

	static final Logger logger = Logger.getLogger(ProdutoController.class);

	
	public ProdutoController(Result result, Validator validator, CategoriaDAO categoriaDAO, UnidadeDAO unidadeDAO,
			ProdutoDAO produtoDAO, SessaoAplicacao sessaoAplicacao,
			Paginador paginador) {
		super();
		this.result = result;
		this.validator = validator;
		this.categoriaDAO = categoriaDAO;
		this.unidadeDAO = unidadeDAO;
		this.produtoDAO = produtoDAO;
		this.sessaoAplicacao = sessaoAplicacao;
		this.paginador = paginador;
	}

	@Permissao(PerfilUsuario.FORNECEDOR)
    @Get(PRODUTO)
	public void formulario(){

		//busca todas as entidades cadastradas
		List<Categoria> listaCategoria = this.categoriaDAO.buscarTodas();
		
		//valida existencia das entidades
		verificarRegistros(listaCategoria, Categoria.class);
		
		//busca todas as entidades cadastradas
		List<Unidade> listaUnidade = this.unidadeDAO.buscarTodas();
		
		//valida existencia de unidades
		verificarRegistros(listaUnidade, Unidade.class);
		
		//se encontrou registro adiciona para visualizaÁ„o
		this.result.include("categoriaList", listaCategoria);
		this.result.include("unidadeList", listaUnidade);

	}

	/**
	 * Lista os resultados dos produtos
	 * @param pagina
	 */
	@Permissao({PerfilUsuario.FORNECEDOR, PerfilUsuario.CLIENTE})
	@Get({PRODUTO_LISTAR_PAGINA, PRODUTO_LISTAR})
	public void listar(Integer pagina){

		logger.info(" pagina selecionada: "+(pagina));

		int totalRegistros = this.produtoDAO.buscarTotalRegistros();

		//realiza o calcula para determinar o valor do range de pesquisa
		paginador.calcular(pagina, this.sessaoAplicacao.getMaximoPorPagina(), totalRegistros);

		//pesquisa pelo range encontrado
		List<Produto> lista = 
				this.produtoDAO.buscarLimitadoPor(paginador.getInicioPesquisa(), paginador.getMaximoPorPagina());

		if(lista.isEmpty()){

			logger.debug(" Nao encontrou registros");

			//informa sobre resultado da operaÁ„o
			this.result.include("message","sem.registros");

			//redireciona para pagina inicial
			this.result.redirectTo(IndexController.class).index();

		} else {

			logger.debug("enviando lista de registros para a requisicao ");

			//adiciona as entidades na requisicao
			this.result.include("produtoList", lista);
			this.result.include("paginador", this.paginador);
		}		
	}	

	/**
	 * Lista os pedidos cadastrados
	 * @param pagina
	 */
	@Permissao({PerfilUsuario.FORNECEDOR, PerfilUsuario.CLIENTE})
	@Get(PRODUTO_ESPECIFICO)
	public void visualizar(Long id){

		logger.debug("visualizar: "+(id));

		this.validator.checking(new NuloValidator(id));
		
		//caso ocorram erros, framework desvia o fluxo para a lista
		//exibindo mensagens de erro
		validator.onErrorRedirectTo(this).listar(1);
		
		Produto produto = this.produtoDAO.buscarPorID(id);

		this.validator.checking(new NuloValidator(produto));
		
		//caso ocorram erros, framework desvia o fluxo para a lista
		//exibindo mensagens de erro
		this.validator.onErrorRedirectTo(this).listar(1);

		//retorna para a pagina com o nome do metodo
		this.result.include("produto", produto);
	}

	/**
	 * Salva a entidade
	 * 
	 * @param produto
	 */
	@Permissao(PerfilUsuario.FORNECEDOR)
	@Post(PRODUTO)
	public void cadastrar(Produto produto){

		logger.info("cadastrar");
		
		this.validacaoIntegridade(produto);
		
		this.validacaoDuplicidade(produto);
		
		produto.setDataAtualizacao(Calendar.getInstance());
		produto.setDataCadastro(Calendar.getInstance());

		this.persistencia(produto);
	}
	
	
	
	/**
	 * Atualiza entidade 
	 * @param id
	 */
	@Permissao(PerfilUsuario.FORNECEDOR)
	@Put(PRODUTO_ESPECIFICO)
	public void atualizar(Produto produto){
		
		logger.info("atualizar");

		validacaoIntegridade(produto);

		validacaoDuplicidade(produto);
		
		//realiza pesquisa para tornar managed
		Produto managed = this.produtoDAO.buscarPorID(produto.getId());
		
		//atualizamos as informacoes no objeto managed para evitar erros do ORM
		managed.setNome(produto.getNome());
		managed.setDescricao(produto.getDescricao());
		managed.setPreco(produto.getPreco());
		managed.setCategoria(produto.getCategoria());
		managed.setUnidade(produto.getUnidade());
		managed.setDataAtualizacao(Calendar.getInstance());
		managed.setDisponivel(produto.isDisponivel());

		persistencia(managed);
	}	
	
	/**
	 * Prepara objeto para edicao
	 * 
	 * @param listId
	 */
	@Permissao(PerfilUsuario.FORNECEDOR)
	@Put(PRODUTO)
	public void prepararParaEdicao(List<Long> listId, Integer pagina){

		logger.debug("prepararParaEdicao: "+(listId));
		
		//realiza regra de selecao de elementos
		this.validator.checking(new SelecaoValidation(listId, true));

		logger.debug("validou selecao");

		//caso ocorram erros, framework desvia o fluxo para a lista
		//exibindo mensagens de erro
		validator.onErrorForwardTo(this).listar(pagina);

		Long id = listId.get(0);

		logger.debug("ID pesquisado: "+(id));

		//busca a entidade pelo seu id
		Produto produto = this.produtoDAO.buscarPorID(id);

		//se encontrar objeto...
		if(produto != null){

			//setamos valor para view
			this.result.include("produto", produto);

			logger.debug("encontrou entidade pelo ID");

			//redireciona para a p·gina de ediÁ„o
			this.result.forwardTo(this).formulario();

		} else {

			logger.debug("registro nao encontrado");

			//informa sobre resultado da operacao
			this.result.include("message","operacao.erro");

			this.result.redirectTo(this).listar(pagina);
		}
	}
	
	/**
	 * Exclui produto
	 * @param listId
	 */
	@Delete(PRODUTO)
	public void remover(List<Long> listId, Integer pagina){

		logger.debug("remover: "+(listId)+" pagina: "+(pagina));
		
		//verifica se houve selecao
		this.validator.checking(new SelecaoValidation(listId, false));

		//se houver erros retorna para a pagina e exibe mensagem
		this.validator.onErrorForwardTo(this).listar(pagina);

		logger.info("validou selecao");
		
		this.produtoDAO.removerEmLote(listId);
		
		//realiza remocao dos itens
		logger.info("removeu itens");

		//informa sobre resultado da operaÁ„o
		this.result.include("message", "operacao.sucesso");
		
		this.result.redirectTo(this).listar(1);
	}


	/**
	 * Cadastra ou atualiza objeto
	 * @param produto
	 */
	private void persistencia(Produto produto) {
		
		logger.info("ira salvar o objeto: "+(produto));
		
		this.produtoDAO.salvar(produto);

		this.result.include("message", "operacao.sucesso");

		//redireciona para o painel do usuário
		this.result.redirectTo(this).listar(1);
	}

	/**
	 * Verifica se as informacoes do objeto ja foram cadastradas
	 * @param produto
	 * @param managed
	 */
	private void validacaoDuplicidade(Produto produto) {
		
		//realiza pesquisa para validar duplicidade
		List<Produto> lista = this.produtoDAO.buscarPorNome(produto.getNome());

		//se encontrou retiramos o registro da lista para validacao
		Produto managed = lista.isEmpty() ? null : lista.get(0);
		
		//checa retorno da consulta 
		this.validator.checking(new NomeDuplicadoValidator(managed, produto));

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro criada pelo validator
		validator.onErrorRedirectTo(this).formulario();

		logger.info("realizou verificacao de duplicidade");
	}

	/**
	 * Valida a existencia de registros e desvia o fluxo em caso de erro
	 * @param lista
	 * @param clazz
	 */
	private void verificarRegistros(List<? extends Entidade> lista, Class<? extends Entidade> clazz) {
		
		//valida se possui entidades
		this.validator.checking(new SemRegistroValidator(lista, clazz));
		
		//se houver erros framework desvia o fluxo
		validator.onErrorUsePageOf(IndexController.class).index();
	}


	/**
	 * Verifica se possui as informacoes obrigatorias
	 * @param produto
	 */
	private void validacaoIntegridade(Produto produto) {
		
		logger.debug(" Id:"+produto.getId()+"  Nome: "+produto.getNome()+" | Descricao: "+produto.getDescricao()+ " | disponivel: "+produto.isDisponivel());

		this.validator.validate(produto);

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro
		validator.onErrorRedirectTo(this).formulario();

		logger.info("realizou validacao de campos");
	}
}

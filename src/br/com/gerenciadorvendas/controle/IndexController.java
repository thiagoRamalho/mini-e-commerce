package br.com.gerenciadorvendas.controle;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
/**
 * 
 * @author Thiago Ramalho
 * Classe que controla o acesso ao indice do usuario
 */
@Resource
public class IndexController {

	private final Result result;

	public IndexController(Result result) {
		this.result = result;
	}

	@Path("/")
	public void index() {
		result.include("variable", "VRaptor!");
	}

}

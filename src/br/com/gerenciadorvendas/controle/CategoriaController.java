package br.com.gerenciadorvendas.controle;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.aplicacao.paginacao.Paginador;
import br.com.gerenciadorvendas.aplicacao.validadores.NomeDuplicadoValidator;
import br.com.gerenciadorvendas.aplicacao.validadores.SelecaoValidation;
import br.com.gerenciadorvendas.modelo.dao.CategoriaDAO;
import br.com.gerenciadorvendas.modelo.entidade.Categoria;
import br.com.gerenciadorvendas.modelo.util.PerfilUsuario;
import br.com.gerenciadorvendas.modelo.util.Permissao;

/**
 * 
 * @author Thiago Ramalho
 * Classe que controla as acoes referentes as categorias
 */
@Permissao(PerfilUsuario.FORNECEDOR)
@Resource
public class CategoriaController {

	private static final String CATEGORIA = "/categoria";
	private static final String CATEGORIA_LISTAR = CATEGORIA+"/listar";
	private static final String CATEGORIA_LISTAR_PAGINA = CATEGORIA_LISTAR+"/{pagina}";
	private static final String CATEGORIA_ESPECIFICA = CATEGORIA+"/{categoria.id}";
	
	private Result result;
	private Validator validator;

	private CategoriaDAO categoriaDAO;
	private SessaoAplicacao sessaoAplicacao;
	private Paginador paginador;

	static final Logger logger = Logger.getLogger(CategoriaController.class);


	public CategoriaController(Result result, Validator validator, CategoriaDAO categoriaDAO, SessaoAplicacao sessaoAplicacao, Paginador paginador) {
		this.result = result;
		this.validator = validator;
		this.categoriaDAO = categoriaDAO;
		this.sessaoAplicacao = sessaoAplicacao;
		this.paginador = paginador;
	}

	@Get(CATEGORIA)
	public void formulario(){}

	/**
	 * Lista os resultados das categorias
	 * @param pagina
	 */
	@Get({CATEGORIA_LISTAR_PAGINA, CATEGORIA_LISTAR})
	public void listar(Integer pagina){

		logger.info(" * pagina selecionada: "+(pagina));

		int totalRegistros = this.categoriaDAO.buscarTotalRegistros();

		//realiza o calcula para determinar o valor do range de pesquisa
		paginador.calcular(pagina, this.sessaoAplicacao.getMaximoPorPagina(), totalRegistros);

		//pesquisa pelo range encontrado
		List<Categoria> lista = 
				this.categoriaDAO.buscarLimitadoPor(paginador.getInicioPesquisa(), paginador.getMaximoPorPagina());

		if(lista.isEmpty()){

			logger.debug(" Nao encontrou registros");

			//informa sobre resultado da operaÁ„o
			this.result.include("message","sem.registros");

			//redireciona para pagina inicial
			this.result.redirectTo(IndexController.class).index();

		} else {

			logger.debug("enviando lista de registros para a requisicao ");

			//adiciona as categorias na requisicao
			this.result.include("categoriaList", lista);
			this.result.include("paginador", this.paginador);
		}		
	}	

	/**
	 * Salva a entidade
	 * 
	 * @param categoria
	 */
	@Post(CATEGORIA)
	public void cadastrar(Categoria categoria){

		logger.info("cadastrar");
		
		this.validacaoIntegridade(categoria);
		
		this.validacaoDuplicidade(categoria);
		
		categoria.setDataAtualizacao(Calendar.getInstance());
		categoria.setDataCadastro(Calendar.getInstance());

		this.persistencia(categoria);
	}
	
	/**
	 * Atualiza objeto 
	 * @param id
	 */
	@Put(CATEGORIA_ESPECIFICA)
	public void atualizar(Categoria categoria){
		
		logger.info("atualizar");

		validacaoIntegridade(categoria);

		validacaoDuplicidade(categoria);
		
		//realiza pesquisa para tornar managed
		Categoria managed = this.categoriaDAO.buscarPorID(categoria.getId());
		
		//atualizamos as informacoes no objeto managed para evitar erros do ORM
		managed.setNome(categoria.getNome());
		managed.setDescricao(categoria.getDescricao());
		managed.setDataAtualizacao(Calendar.getInstance());

		persistencia(managed);
	}	
	
	/**
	 * Prepara objeto para visualizacao
	 * 
	 * @param listId
	 */
	@Put(CATEGORIA)
	public void prepararParaEdicao(List<Long> listId){

		logger.debug("prepararParaEdicao");
		
		//realiza regra de selecao de elementos
		this.validator.checking(new SelecaoValidation(listId, true));

		logger.debug("validou selecao");

		//caso ocorram erros, framework desvia o fluxo para a lista
		//exibindo mensagens de erro
		validator.onErrorUsePageOf(this).listar(1);

		Long id = listId.get(0);

		logger.debug("ID pesquisado: "+(id));

		//busca a categoria pelo seu id
		Categoria categoria = this.categoriaDAO.buscarPorID(id);

		//se encontrar objeto...
		if(categoria != null){

			//setamos valor para view
			this.result.include("categoria", categoria);

			logger.debug("encontrou entidade pelo ID");

			//redireciona para a p·gina de ediÁ„o
			this.result.forwardTo(this).formulario();

		} else {

			logger.debug("registro nao encontrado");

			//informa sobre resultado da operacao
			this.result.include("message","operacao.erro");

			this.result.redirectTo(this).listar(1);
		}
	
	}

	/**
	 * Cadastra ou atualiza objeto
	 * @param categoria
	 */
	private void persistencia(Categoria categoria) {
		
		logger.info("ira salvar o objeto: "+(categoria));
		
		this.categoriaDAO.salvar(categoria);

		this.result.include("message", "operacao.sucesso");
		
		//redireciona para o painel do usuário
		this.result.redirectTo(this).listar(1);
	}

	/**
	 * Verifica se as informacoes do objeto ja foram cadastradas
	 * @param categoria
	 * @param managed
	 */
	private void validacaoDuplicidade(Categoria categoria) {
		
		//realiza pesquisa para validar duplicidade
		List<Categoria> lista = this.categoriaDAO.buscarPorNome(categoria.getNome());

		//se encontrou retiramos o registro da lista para validacao
		Categoria managed = lista.isEmpty() ? null : lista.get(0);
		
		//checa retorno da consulta 
		this.validator.checking(new NomeDuplicadoValidator(managed, categoria));

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro criada pelo validator
		validator.onErrorUsePageOf(this).formulario();

		logger.info("realizou verificacao de duplicidade");
	}

	/**
	 * Verifica se possui as informacoes obrigatorias
	 * @param categoria
	 */
	private void validacaoIntegridade(Categoria categoria) {
		
		logger.debug(" Id:"+categoria.getId()+"  Nome: "+categoria.getNome()+" | Descricao: "+categoria.getDescricao());

		this.validator.validate(categoria);

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro
		validator.onErrorUsePageOf(this).formulario();

		logger.info("realizou validacao de campos");
	}
}

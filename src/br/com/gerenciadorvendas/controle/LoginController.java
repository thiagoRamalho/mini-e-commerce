package br.com.gerenciadorvendas.controle;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.gerenciadorvendas.aplicacao.SessaoAplicacao;
import br.com.gerenciadorvendas.aplicacao.validadores.LoginValidator;
import br.com.gerenciadorvendas.modelo.dao.UsuarioDAO;
import br.com.gerenciadorvendas.modelo.entidade.Usuario;
import br.com.gerenciadorvendas.modelo.util.Publico;

/**
 * 
 * @author Shark
 * Controller responsável pelo acesso do usuário ao sistema
 */
@Resource
public class LoginController {

	private final Result result;
	private Validator validator;
	private SessaoAplicacao sessaoAplicacao;
	
	static final Logger logger = Logger.getLogger(LoginController.class);
	private UsuarioDAO usuarioDAO;
	
	/**
	 * 
	 * @param result
	 * @param validator
	 * @param sessaoAplicacao
	 * @param usuarioDAO
	 */
	public LoginController(Result result,  Validator validator, SessaoAplicacao sessaoAplicacao, UsuarioDAO usuarioDAO) {
		this.result = result;
		this.validator = validator;
		this.sessaoAplicacao = sessaoAplicacao;
		this.usuarioDAO = usuarioDAO;
	}

	/**
	 * Redireciona para a pagina do formulario
	 */
	@Publico
	@Get("/login")
	public void login() {}
	
	/**
	 * Metodo que recebe o resultado do submit da pagina
	 * de login e inicia processo de autenticacao 
	 * 
	 * @param users
	 */
	@Publico
	@Post("/login")
	public void login(Usuario usuario){

		logger.debug("Nick: "+usuario.getNickName()+" | Senha: "+usuario.getSenha());

		this.validator.validate(usuario);

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro
		validator.onErrorUsePageOf(this).login();

		logger.info("realizou validacao de campos");
	
		usuario = this.usuarioDAO.buscarPorNickSenha(usuario.getNickName(), usuario.getSenha());
		
		//checa retorno da consulta por usuário
		this.validator.checking(new LoginValidator(usuario));

		//caso ocorram erros, framework desvia o fluxo para o form
		//exibindo mensagens de erro
		validator.onErrorUsePageOf(this).login();

		logger.info("realizou verificacao de login no sistema");
		
		//adiciona objeto clonado para desvicular estado do ORM
		this.sessaoAplicacao.setUsuario(usuario.clone());
		
		//redireciona para o painel do usuário
		this.result.redirectTo(IndexController.class).index();
	}
	
	@Get("/logout")
	public void logout(){		
		//reseta objeto de sessao
		this.sessaoAplicacao.logout();
		this.result.redirectTo(this).login();
	}
}
